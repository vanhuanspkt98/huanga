﻿
           $(document).ready(function () {
               var that = this;
               var crudServiceBaseUrl = "/Store",
                   dataSource = new kendo.data.DataSource({
                       transport: {
                           read: {
                               url: crudServiceBaseUrl + "/GetGrid",
                               dataType: "json"
                           },
                           update: {
                               url: crudServiceBaseUrl + "/Update",
                               dataType: "json",
                               complete: function (data) {
                                   location.reload();
                               }
                           },
                           destroy: {
                               url: crudServiceBaseUrl + "/Destroy",
                               dataType: "json",
                               complete: function (data) {
                                   location.reload();
                               }
                           },
                           create: {
                               url: crudServiceBaseUrl + "/Create",
                               dataType: "json",
                               complete: function (data) {
                                   location.reload();
                               }
                           },
                           //aggregate: [
                           // { field: "Price", aggregate: "sum" }
                           //],
                           parameterMap: function (options, operation) {
                               if (operation !== "read" && options.models) {
                                   return { models: kendo.stringify(options.models) };
                               }
                           }
                       },
                       batch: true,
                       pageSize: 20,
                       schema: {
                           model: {
                               id: "StoreID",
                               fields: {
                                   StoreID: { editable: false, nullable: false, hidden: true },
                                   StoreName: { editable: true },
                                   StoreAddress: { editable: true },
                                   PhoneNumber: { type: "number" },
                                   Website: {  },
                               }
                           }
                       }

                   });

               $("#grid").kendoGrid({
                   dataSource: dataSource,
                   navigatable: true,
                   selectable: true,
                   //columnResizeHandleWidth: 6,
                   allowCopy: true,
                   pageable: true,
                   groupable: true,
                   sortable: true,
                   reorderable: true,
                   resizable: true,
                   height: 550,
                   toolbar: [{ name: "create", text: "Thêm Cửa Hàng Mới"}],
                   columns: [
                     //  { field: "StoreID", title: "Mã Cửa Hàng" },
                       { field: "StoreName", title: "Tên Cửa Hàng", width: "120px" },
                        { field: "StoreAddress", title: "Địa Chỉ", width: "120px" },
                        { field: "PhoneNumber", title:"Số Điện Thoại" ,width:"120px"},
                       { field: "Website", title: "Website", width: "120px" },
                       {
                           command: [{
                               name: "edit",
                               text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                           },
                           {
                               name: "destroy",
                               text: "Xóa"
                           }
                           ], title: "&nbsp;", width: "250px"
                       }],
                   editable: {
                       mode: "popup",
                       confirmation: function (e) {
                           return "Bạn có muốn xóa Cửa Hàng " + e.StoreID + "?";
                       },
                       window: {
                           title: "Thêm/ Cập Nhật Cửa Hàng",

                       }
                       // confirmation: true,
                       //cancelDelete: "No"
                       // confirmDelete: "Có"
                   }
                   // refresh:true
               });
           });
