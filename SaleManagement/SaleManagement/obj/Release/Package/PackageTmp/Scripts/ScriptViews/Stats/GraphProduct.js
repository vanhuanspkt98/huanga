﻿
           $("#day").kendoDatePicker({
               start: "month",
               depth: "month",
               format: "MM/dd/yyyy"
           });
$("#month").kendoDatePicker({
    start: "year",
    depth: "year",
    format: "MM/yyyy"
});
$("#year").kendoDatePicker({
    start: "decade",
    depth: "decade",
    format: "yyyy"
});
// create DropDownList from input HTML element
$("#categoryDay").kendoDropDownList({
    dataTextField: "CategoryName",
    dataValueField: "CategoryID",
    dataSource: {
        transport: {
            read: {
                url: "/category/GetGrid",
                dataType: "json"
            }
        }
    },
    index: 0,
    change: onChange
});
function onChange() {
    var value = $("#categoryDay").val();
};
// create DropDownList from input HTML element
$("#categoryMonth").kendoDropDownList({
    dataTextField: "CategoryName",
    dataValueField: "CategoryID",
    dataSource: {
        transport: {
            read: {
                url: "/category/GetGrid",
                dataType: "json"
            }
        }
    },
    index: 0,
    change: onChange
});
function onChange() {
    var value = $("#categoryMonth").val();
};
// create DropDownList from input HTML element
$("#categoryYear").kendoDropDownList({
    dataTextField: "CategoryName",
    dataValueField: "CategoryID",
    dataSource: {
        transport: {
            read: {
                url: "/category/GetGrid",
                dataType: "json"
            }
        }
    },
    index: 0,
    change: onChange
});
function onChange() {
    var value = $("#categoryYear").val();
};

 
            function load() {
                var chart = $("#chart").data("kendoChart");
                chart.dataSource.read();
                chart.refresh();
            };
    
    
        $('#refresh').click(function () {
            var chart = $("#chart").data("kendoChart");
            chart.dataSource.read();
            chart.refresh();
        });
    
    
        var date = new Date();
    $("#chart").kendoChart({
        dataSource: {
            transport: {
                read: {
                    url: "/Stats/GraphProductByYear?year=" + date.getFullYear() ,
                    dataType: "json"
                }
            }
        },
        title: {
            text: "Biểu Đồ Thống Kê Sản Lượng Sản Phẩm Bán Ra Trong Năm "
        },
        legend: {
            visible: true,
            position: "top"
        },
        series: [{
            name: "Số Lượng",
            data: "dataSource",
            type: "column",
            // style: "smooth",
            field: "Quantity",
            color: "rgb(60,141,188)",
            categoryField: "ProductName",
            gap: 0.2,
            spacing: 0.2,
            markers: {
                visible: false
            }
        }
        ],
        categoryAxis: {
            categories: {
                field:"ProductName"
            },
            majorGridLines: {
                visible: true
            }
        },
        tooltip: {
            visible: true,
            format: "{0}%",
            template: "#= series.name #: #= value #"
        }
    });
    

    
        function GraphOrderYear() {
            var value1 = $('#year').val();
            var cate = $('#categoryYear').val();
            $("#chart").kendoChart({
                dataSource: {
                    transport: {
                        read: {
                            url: "/Stats/GraphProductByYear?year=" + value1+"&&category="+cate,
                            dataType: "json"
                        }
                    }
                },
                title: {
                    text: "Biểu Đồ Thống Kê Sản Lượng Sản Phẩm Bán Ra Trong Năm "
                },
                legend: {
                    visible: true,
                    position: "top"
                },
                seriesDefault: {
                    type: "line",
                    style: "smooth",
                    stack: true
                },
                series: [{
                    name: "Số Lượng",
                    data: "dataSource",
                    type: "column",
                    // style: "smooth",
                    field: "Quantity",
                    color: "green",
                    categoryField: "ProductName",
                    gap: 0.2,
                    spacing: 0.2,
                    markers: {
                        visible: false
                    }
                }
                ],
                categoryAxis: {
                    //categories: ["1", "2", "3", "4", "5", "7", "8", "9", "10", "11", " 12", "13", "14", "15", "16", "18", "19", "20", "21", "22", "23", "24", "25", "26","27", "28", "29", "30", "31"],
                    categories: [" Một", " Hai", " Ba", " Tư", " Năm", " Sáu", " Bảy", " Tám", " Chín", " Mười", " Mười Một", " Mười Hai"],
                    majorGridLines: {
                        visible: false
                    },
                    majorGridLines: {
                        visible: false
                    },
                    majorTicks: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= series.name #: #= value #"
                }
            });
        }
    
    
        function GraphOrderMonth() {
            var value1 = $('#month').val();
            var cate = $('#categoryMonth').val();
            $("#chart").kendoChart({
                dataSource: {
                    transport: {
                        read: {
                            url: "/Stats/GraphProductByMonth?month=" + value1 +"&&category="+cate,
                            dataType: "json"
                        }
                    }
                },
                title: {
                    text: "Biểu Đồ Thống Kê Sản Lượng Sản Phẩm Bán Ra Trong Tháng "
                },
                legend: {
                    visible: true,
                    position: "top"
                },
                seriesDefault: {
                    type: "area",
                    style: "smooth",
                    stack: true
                },
                series: [{
                    name: "Số Lượng",
                    data: "dataSource",
                    type: "column",
                    // style: "smooth",
                    field: "Quantity",
                    color: "green",
                    categoryField: "ProductName",
                    gap: 0.2,
                    spacing: 0.2,
                    markers: {
                        visible: false
                    }
                }
                ],
                categoryAxis: {
                    categories: ["1", "2", "3", "4","5", "6", "7", "8", "9", "10", "11", " 12", "13", "14", "15", "16","17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                    majorGridLines: {
                        visible: false
                    },
                    majorGridLines: {
                        visible: false
                    },
                    majorTicks: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= series.name #: #= value #"
                }

            });
        }
    
    
        function GraphOrderDay() {
            var value1 = $('#day').val();
            var cate = $('#categoryDay').val();
            $("#chart").kendoChart({
                dataSource: {
                    transport: {
                        read: {
                            url: "/Stats/GraphProductByDay?day=" + value1+"&&category="+cate,
                            dataType: "json"
                        }
                    }
                },
                title: {
                    text: "Biểu Đồ Thống Kê Sản Lượng Sản Phẩm Bán Ra Trong Ngày "
                },
                legend: {
                    visible: true,
                    position: "top"
                },
                seriesDefault: {
                    type: "line",
                    style: "smooth",
                    stack: true
                },
                series: [{
                    name: "Số Lượng",
                    data: "dataSource",
                    type: "column",
                    // style: "smooth",
                    field: "Quantity",
                    color: "green",
                    categoryField: "ProductName",
                    gap: 0.2,
                    spacing: 0.2,
                    markers: {
                        visible: false
                    }
                }
                ],
                categoryAxis: {
                    categories: ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
                    majorGridLines: {
                        visible: false
                    },
                    majorGridLines: {
                        visible: false
                    },
                    majorTicks: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= series.name #: #= value #"
                }

            });
        }
    
