﻿

    var date = new Date();
$("#chart").kendoChart({
    dataSource: {
        transport: {
            read: {
                url: "/Stats/LiquidityOfWeek",
                dataType: "json"
            }
        }
    },
    title: {
        //  text: "Biểu Đồ Thống Kê Doanh Thu Trong Năm "
    },
    //panes: [
    //    { name: "top-pane" },
    //    { name: "left-pane" }
    //],
    //plotArea: {
    //    background: "/Content/img/back.png",

    //},
    chartArea: {
        width: 350,
        height: 350
        //    border: {
        //        width: 2,
        //        color: "transparent",
        //       // dashType: "solid",
        //        height: 200,
        //        width: 500,
        //        margin: {
        //            top: 10,
        //            left: 10,
        //            right: 10,
        //            bottom: 10,
        //            opacity: 0.1
        //        }
        //    }
    },
    legend: {
        visible: true,
        align: "center",
        //   offsetX: 240,
        // offsetY: 230,
        //  background: "#ADB3BC",
        position: "top",
        //labels: {
        //    font: "20px sans-serif",
        //    color: "red"
        //}
    },
    series: [{
        name: "Tuần trước",
        data: "dataSource",
        type: "area",
        // style:"smooth",
        field: "LiquidityLastWeek",
        color: "rgb(162,162,162)",//chinh mau do thi tuan truoc
        categoryField: "Time",
        markers: {
            visible: false
        }
    },
    {
        name: "Tuần này",
        data: "dataSource",
        type: "line",
        // closeField: "Time",
        // highField: "Time",
        //   style: "smooth",
        // visible:false,
        //  noteTextField: "Time",
        //notes: {
        //    icon: {
        //        background: "green"
        //    }
        //},
        field: "Liquidity",
        color: "green",
        categoryField: "Time",
        //gap: 0.2,
        //spacing: 0.2,
        markers: {
            visible: false
        }
    }

    ],
    categoryAxis: {
        // categories: ["", " Hai", " Ba", " ", " Năm", " Sáu", " Bảy", " Tám", " Chín", " Mười", " Mười Một", " Mười Hai"],
        majorGridLines: {
            visible: false
        },
        majorTicks: {
            visible: false
        }
    },
    valueAxis: {
        //title: {
        //    text: "voltage"
        //},
        majorGridLines: {
            visible: false
        },
        visible: false
            
    },
    tooltip: {
        visible: true,
        format: "{0}%",
        template: " #= value #"
    }
});


    var date = new Date();
$("#chart2").kendoChart({
    dataSource: {
        transport: {
            read: {
                url: "/Stats/TurnoverOfWeek",
                dataType: "json"
            }
        }
    },
    title: {
        //  text: "Biểu Đồ Thống Kê Doanh Thu Trong Năm "
    },
    //panes: [
    //    { name: "top-pane" },
    //    { name: "left-pane" }
    //],
    //plotArea: {
    //    background: "white",

    //},
    chartArea: {
        width: 350,
        height: 352
    },
    chartArea: {
        //border: {
        //    width: 2,
        //    color: "transparent",
        //   // dashType: "solid",
        //    height: 352,
        //    width: 350,
        //    margin: {
        //        top: 10,
        //        left: 10,
        //        right: 10,
        //        bottom: 10,
        //        opacity: 0.1
        //    }
        //},
        background: "transparent",
        width: 350,
        height: 352
    },
    legend: {
        visible: true,
        // align: "center",
        //offsetX: 240,
        //offsetY: 230,
        //  background: "green",
        position: "top",
        //labels: {
        //    font: "20px sans-serif",
        //    color: "red"
        //}
    },
    seriesDefaults: {
    type: "line"
        },
    series: [{
        name: "Tuần trước",
        // style:"smooth",
        data: "dataSource",
        type: "area",
        field: "TurnoverLastWeek",
        color: "rgb(102,102,102)",
        categoryField: "Time",
        markers: {
            visible: false
        }
    }, {
        name: "Tuần này",
        data: "dataSource",
        type: "line",
        // closeField: "Time",
        // highField: "Time",
        //    style: "smooth",
        // visible:false,
        //  noteTextField: "Time",
        //notes: {
        //    icon: {
        //        background: "green"
        //    }
        //},
        field: "Turnover",
        color: "rgb(247, 154, 10)",
        categoryField: "Time",
        //gap: 0.2,
        //spacing: 0.2,
        markers: {
            visible: false
        }
    }

    ],
    categoryAxis: {
        // categories: ["", " Hai", " Ba", " ", " Năm", " Sáu", " Bảy", " Tám", " Chín", " Mười", " Mười Một", " Mười Hai"],
        majorGridLines: {
            visible: false
        },
        majorTicks: {
            visible: false
        }
    },
    valueAxis: {
        //title: {
        //    text: "voltage"
        //},
        majorGridLines: {
            visible: false
        },
        visible: false
    },
    tooltip: {
        visible: true,
        format: "{0}%",
        template: " #= value #"
    }
});
