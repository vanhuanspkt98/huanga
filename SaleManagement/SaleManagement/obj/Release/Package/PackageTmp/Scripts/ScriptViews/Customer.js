﻿
          $(document).ready(function () {
              var that = this;
              var crudServiceBaseUrl = "/Customer",
                  dataSource = new kendo.data.DataSource({
                      transport: {
                          read: {
                              url: crudServiceBaseUrl + "/GetGrid",
                              dataType: "json"
                          },
                          update: {
                              url: crudServiceBaseUrl + "/Update",
                              dataType: "json",
                              complete: function (data) {
                                  $("#grid").data("kendoGrid").dataSource.read();
                              }
                          },
                          destroy: {
                              url: crudServiceBaseUrl + "/Destroy",
                              dataType: "json",
                              complete: function (data) {
                                  $("#grid").data("kendoGrid").dataSource.read();
                              }
                          },
                          create: {
                              url: crudServiceBaseUrl + "/Create",
                              dataType: "json",
                              complete: function (data) {
                                  $("#grid").data("kendoGrid").dataSource.read();
                              }
                          },
                          //aggregate: [
                          // { field: "Price", aggregate: "sum" }
                          //],
                          parameterMap: function (options, operation) {
                              if (operation !== "read" && options.models) {
                                  var str = JSON.stringify(options.models);
                                  var lastStr = str.lastIndexOf("{");
                                  var str = str.substring(lastStr);
                                  var tmp = "[";
                                  str = tmp.concat(str);
                                     
                                  return { models: str };
                              }
                          }
                      },
                      batch: true,
                      pageSize: 20,
                      schema: {
                          model: {
                              id: "CustomerID",
                              fields: {
                                  CustomerID: { editable: false, nullable: false, hidden: true },
                                  Email: {type:"email", validation: { required: true } },
                                  LastName: { validation: { required: true } },
                                  MiddleName: { },
                                  FirstName: { validation: { required: true } },
                                  Birthday: { type:"date",validation: { required: true } },// set datetime type
                                  Address: { type:"address", validation: { required: true } },
                                  Sex: { type: "boolean"},
                                  PhoneNumber: { type: "number", validation: { required: true, min: 0 } },
                                  CustomerCategoryID: { editable: true },
                                  
                              }
                          }
                      }

                  });

              $("#grid").kendoGrid({
                  dataSource: dataSource,
                  navigatable: true,
                  selectable: true,
                  //columnResizeHandleWidth: 6,
                  allowCopy: true,
                  pageable: true,
                  groupable: true,
                  sortable: true,
                  reorderable: true,
                  resizable: true,
                  height: 550,
                  toolbar: [{ name: "create", text: "Thêm Khách Hàng Mới"}],
                  columns: [
                    //  { field: "CustomerID", title: "Mã Khách Hàng" ,hidden:true},
                      {
                          field: "CustomerCategoryID", title: "Loại Khách Hàng", hidden: true,
                          editor: function (container) {
                              var input = $('<input id="CustomerID" name="CustomerCategoryID">');
                              // append to the editor container
                              input.appendTo(container);

                              // initialize a dropdownlist
                              input.kendoDropDownList({
                                  dataTextField: "CustomerCategoryName",
                                  dataValueField: "CustomerCategoryID",
                                  dataSource: new kendo.data.DataSource({
                                      transport: {
                                          read: {
                                              url: crudServiceBaseUrl + "/GetgridCusCate",
                                              dateType: "json"
                                          },
                                          parameterMap: function (options, operation) {
                                              if (operation !== "read" && options.models) {
                                                  return { models: kendo.stringify(options.models) };
                                              }
                                          }
                                      },
                                      schema: {
                                          model: {
                                              id: "CustomerCategoryID",
                                              fields: {
                                                  CustomerCategoryID: {},
                                                  CustomerCategoryName: {},
                                              }
                                          }
                                      }
                                  }) // bind it to the brands array
                              }).appendTo(container);
                          }
                      },
                        
                      { field: "LastName", title: "Họ", width: "70px" },
                      { field: "MiddleName", title: "Tên Đệm", width: "70px" },
                      { field: "FirstName", title: "Tên", width: "70px" },
                      { field: "Birthday", title: "Ngày Sinh", width: "120px", template: '#= kendo.toString(Birthday,"MM/dd/yyyy") #' },
                       { field: "Sex", title: "Giới Tính", width: "120px", template: "#= Sex ? 'Nam' : 'Nữ' #" },
                      { field: "Address", title: "Địa Chỉ", width: "120px" },
                      { field: "Email", title: "Email",width:"120px" },
                      { field: "PhoneNumber", title: "Số Điện Thoại", width: "120px" },
                      //  { field: "Point", title: "Điểm TL", width: "80px" ,hiden:true},

                      {
                          command: [{
                              name: "edit",
                              text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                          },
                          {
                              name: "destroy",
                              text: "Xóa"
                          }
                          ], title: "&nbsp;", width: "250px"
                      }],
                  editable: {
                      mode: "popup",
                      confirmation: function (e) {
                          return "Bạn có muốn xóa Khách Hàng " + e.LastName+" "+e.FirstName +"?";
                      },
                      window: {
                          title: "Thêm/ Cập Nhật Khách Hàng",

                      }
                      // confirmation: true,
                      //cancelDelete: "No"
                      // confirmDelete: "Có"
                  }
                  // refresh:true
              });
          });

