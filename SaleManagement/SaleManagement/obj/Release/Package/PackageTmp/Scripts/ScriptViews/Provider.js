﻿         $(document).ready(function () {
             var that = this;
             var crudServiceBaseUrl = "/Provider",
                 dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: crudServiceBaseUrl + "/GetGrid",
                             dataType: "json"
                         },
                         update: {
                             url: crudServiceBaseUrl + "/Update",
                             dataType: "json",
                             complete: function (data) {
                                 $("#grid").data("kendoGrid").dataSource.read();
                             }
                                
                         },
                         destroy: {
                             url: crudServiceBaseUrl + "/Destroy",
                             dataType: "json",
                             complete: function (data) {
                                 $("#grid").data("kendoGrid").dataSource.read();
                             }
                         },
                         create: {
                             url: crudServiceBaseUrl + "/Create",
                             dataType: "json",
                             complete: function (e) {
                                 $("#grid").data("kendoGrid").dataSource.read();
                             }
                         },
                         //aggregate: [
                         // { field: "Price", aggregate: "sum" }
                         //],
                         parameterMap: function (options, operation) {
                             if (operation !== "read" && options.models) {
                                 var str = JSON.stringify(options.models);
                                 var lastStr = str.lastIndexOf("{");
                                 var str = str.substring(lastStr);
                                 var tmp = "[";
                                 str = tmp.concat(str);
                                     
                                 return { models: str };
                             }
                         }
                     },
                     batch: true,
                     pageSize: 20,
                     schema: {
                         model: {
                             id: "ProviderID",
                             fields: {
                                 ProviderID: { editable: false, nullable: false, hidden: true },
                                 ProviderName: { editable: true },
                                 ProviderAddress: { editable: true },
                                 PhoneNumber: { type: "number", validation: { required: true, min: 0 } },
                                 Email: { editable: true },
                             }
                         }
                     }

                 });

             $("#grid").kendoGrid({
                 dataSource: dataSource,
                 navigatable: true,
                 selectable: true,
                 //columnResizeHandleWidth: 6,
                 allowCopy: true,
                 pageable: true,
                 groupable: true,
                 sortable: true,
                 reorderable: true,
                 resizable: true,
                 height: 550,
                 toolbar: [{ name: "create", text: "Thêm Nhà Cung Cấp Mới"}],
                 columns: [
                  //   { field: "ProviderID", title: "Mã Nhà Cung Cấp" },
                     { field: "ProviderName", title: "Tên Nhà Cung Cấp", width: "120px" },
                     { field: "ProviderAddress", title: "Địa Chỉ Nhà Cung Cấp", width: "120px" },
                     { field: "PhoneNumber", title: "Số Điện Thoại",width:"120px" },
                     { field: "Email", title: "Email", width: "120px" },
                     {
                         command: [{
                             name: "edit",
                             text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                         },
                         {
                             name: "destroy",
                             text: "Xóa"
                         }
                         ], title: "&nbsp;", width: "250px"
                     }],
                 editable: {
                     mode: "popup",
                     confirmation: function (e) {
                         return "Bạn có muốn xóa Nhà Cung Cấp " + e.ProviderName + "?";
                     },
                     window: {
                         title: "Thêm/ Cập Nhật Nhà Cung Cấp",

                     }
                     // confirmation: true,
                     //cancelDelete: "No"
                     // confirmDelete: "Có"
                 }
                 // refresh:true
             });
         });