﻿
    
    $(document).ready(function () {
        function startChange() {
            var startDate = start.value(),
            endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.max(new Date(endDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
        }

        function endChange() {
            var endDate = end.value(),
            startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.max(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
        }

        var start = $("#start").kendoDatePicker({
            change: startChange
        }).data("kendoDatePicker");
    
        var end = $("#end").kendoDatePicker({
            change: endChange
        }).data("kendoDatePicker");

     

        start.max(end.value());
        end.min(start.value());
    });

var url;
function xemlaihoadon() {
    window.location.href = url;
}
$(document).ready(function () {
    var date = new Date();
    var value1 = date.getUTCMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
    var windowTemplate = kendo.template($("#windowTemplate").html());

    dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/Stats/GetDatabyday?day=" + value1,
                dataType: "json"
            },
            update: {
                url: "",
                dataType: "json"
            },
            destroy: {
                url: "/Bill/Destroy",
                dataType: "json"
            },
            create: {
                url: "",
                dataType: "json"
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    var str = JSON.stringify(options.models);
                    var lastStr = str.lastIndexOf("{");
                    var str = str.substring(lastStr);
                    var tmp = "[";
                    str = tmp.concat(str);

                    return { models: str };
                }
            }
        },
        batch: true,
        pageSize: 7,
        schema: {
            model: {
                id: "OrderID",
                fields: {
                    OrderID: { type: "number" },
                    Total: { type: "number" },
                    SumOfProductQuantity: { type: "number" },
                    Time: { type: "string" }

                }
            }
        },
        aggregate: [
            { field: "Time", aggregate: "count" },
            { field: "Total", aggregate: "sum" },
            { field: "Total", aggregate: "min" },
            { field: "Total", aggregate: "max" },
            { field: "SOPQ", aggregate: "sum" }
        ]
    });

    var window = $("#window").kendoWindow({
        title: "Thông tin hóa đơn",
        visible: false, //the window will not appear before its .open method is called
        width: "400px",
        height: "200px",
    }).data("kendoWindow");


    var grid = $("#grid").kendoGrid({
        dataSource: dataSource,
        pageable: true,
        height: 430,
        // toolbar: ["create"],
        columns: [
       {
           field: "Time", title: "Ngày Lập Hóa Đơn", aggregates: ["count"],
           footerTemplate: "Tổng Cộng: #=count#",
           groupFooterTemplate: "Tổng Cộng: #=count#"
       },
                {
                    field: "Total", title: "Tổng Tiền", format: "{0:c}", aggregates: ["sum", "min", "max"],
                    footerTemplate: "<div>Nhỏ nhất: #= min #</div><div>Lớn Nhất: #= max #</div><div>Tổng cộng: #= sum #</div>",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
                {
                    field: "SOPQ", title: "Số Lượng Sản Phẩm", aggregates: ["sum"],
                    footerTemplate: "Tổng Cộng: #=sum#",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
        {
            command: [
              {
                  name: "Delete", text: "CHI TIẾT",
                  click: function (e) {  //add a click event listener on the delete button
                    //  grid.dataSource.read();
                      var tr = $(e.target).closest("tr"); //get the row for deletion
                      // alert(tr.toSource())
                      //console.log(tr);

                      try {
                          var data = this.dataItem(tr); //get the row data so it can be referred later
                          //  alert(data.toSource())
                          //console.log(data);
                          url = "/bill/details?id=" + data.OrderID;
                          window.content(windowTemplate(data)); //send the row data object to the template and render it
                          window.open().center();

                      }
                      catch (e) {
                          //alert("Quý khách vui lòng chọn lại ngày cần thống kê"
                          //    + "\n Trang sẽ được tải lại trong giây lát!"
                          //    + "\n Xin cảm ơn và chúc quý khách một ngày làm việc vui vẻ! "
                          //    + "\n THEPHUONGSOLUTION COMPANY LIMITED ")
                      }

                      $("#yesButton").click(function () {
                          grid.dataSource.remove(data)  //prepare a "destroy" request
                          grid.dataSource.sync()  //actually send the request (might be ommited if the autoSync option is enabled in the dataSource)
                          window.close();
                      })
                      $("#noButton").click(function () {
                          window.close();
                      })
                  }
              }
            ]
        }],
        editable: {
            mode: "popup"

        }
    }).data("kendoGrid");
});

function selectday() {
    var from = $('#start').val();
    //alert(from);
    var to = $('#end').val();
    //alert(to);
    var windowTemplate = kendo.template($("#windowTemplate").html());

    dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/Stats/GetDatadaybyday?from=" + from+"&&to="+to,
                dataType: "json"
            },
            update: {
                url: "",
                dataType: "json"
            },
            destroy: {
                url: "/Bill/Destroy",
                dataType: "json"
            },
            create: {
                url: "",
                dataType: "json"
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    var str = JSON.stringify(options.models);
                    var lastStr = str.lastIndexOf("{");
                    var str = str.substring(lastStr);
                    var tmp = "[";
                    str = tmp.concat(str);

                    return { models: str };
                }
            }
        },
        batch: true,
        pageSize: 7,
        schema: {
            model: {
                id: "OrderID",
                fields: {
                    OrderID: { type: "number" },
                    Total: { type: "number" },
                    SumOfProductQuantity: { type: "number" },
                    Time: { type: "string" }

                }
            }
        },
        aggregate: [
            { field: "Time", aggregate: "count" },
            { field: "Total", aggregate: "sum" },
            { field: "Total", aggregate: "min" },
            { field: "Total", aggregate: "max" },
            { field: "SumOfProductQuantity", aggregate: "sum" }
        ]
    });

    var window = $("#window").kendoWindow({
        title: "Thông tin hóa đơn?",
        visible: false, //the window will not appear before its .open method is called
        width: "400px",
        height: "200px",
    }).data("kendoWindow");


    var grid = $("#grid").kendoGrid({
        dataSource: dataSource,
        pageable: true,
        height: 430,
        // toolbar: ["create"],
        columns: [
       {
           field: "Time", title: "Ngày Lập Hóa Đơn", aggregates: ["count"],
           footerTemplate: "Tổng Cộng: #=count#",
           groupFooterTemplate: "Tổng Cộng: #=count#"
       },
                {
                    field: "Total", title: "Tổng Tiền", format: "{0:c}", aggregates: ["sum", "min", "max"],
                    footerTemplate: "<div>Nhỏ nhất: #= min #</div><div>Lớn Nhất: #= max #</div><div>Tổng cộng: #= sum #</div>",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
                {
                    field: "SumOfProductQuantity", title: "Số Lượng Sản Phẩm", aggregates: ["sum"],
                    footerTemplate: "Tổng Cộng: #=sum#",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
        {
            command: [
              {
                  name: "Delete", text: "CHI TIẾT",
                  click: function (e) {  //add a click event listener on the delete button
                      var tr = $(e.target).closest("tr"); //get the row for deletion
                      // alert(tr.toSource())
                      

                      try {
                          var data = this.dataItem(tr); //get the row data so it can be referred later
                          //  alert(data.toSource())
                          url = "/bill/details?id=" + data.OrderID;
                          window.content(windowTemplate(data)); //send the row data object to the template and render it
                          window.open().center();
                         // alert();

                      }
                      catch (e) {
                          //alert("Quý khách vui lòng chọn lại ngày cần thống kê"
                          //    + "\n Trang sẽ được tải lại trong giây lát!"
                          //    + "\n Xin cảm ơn và chúc quý khách một ngày làm việc vui vẻ! "
                          //    + "\n THEPHUONGSOLUTION COMPANY LIMITED ")
                      }

                      $("#yesButton").click(function () {
                          grid.dataSource.remove(data)  //prepare a "destroy" request
                          grid.dataSource.sync()  //actually send the request (might be ommited if the autoSync option is enabled in the dataSource)
                          window.close();
                      })
                      $("#noButton").click(function () {
                          window.close();
                      })
                  }
              }
            ]
        }],
        editable: {
            mode: "popup"

        }
    }).data("kendoGrid");

}
