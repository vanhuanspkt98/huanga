﻿

     $(document).ready(function () {
         var that = this;
         var imgEmp = "avatar";
         var windowTemplate = kendo.template($("#windowTemplate").html());
         var window = $("#window").kendoWindow({
             title: "<div class='text-center'>CẬP NHẬT HÌNH ẢNH NHÂN VIÊN</div>",
             visible: false, //the window will not appear before its .open method is called
             width: "712px",
             height: "300px",
         }).data("kendoWindow");
         var crudServiceBaseUrl = "/account",
             dataSource = new kendo.data.DataSource({
                 transport: {
                     read: {
                         url: crudServiceBaseUrl + "/GetGrid",
                         dataType: "json"
                     },
                     update: {
                         url: crudServiceBaseUrl + "/Update",
                         dataType: "json",
                         complete: function (data) {
                             $("#grid").data("kendoGrid").dataSource.read();
                             $("#grid").data('kendoGrid').refresh();
                         }
                     },
                     destroy: {
                         url: crudServiceBaseUrl + "/Destroy",
                         dataType: "json",
                         complete: function (e) {
                             $("#grid").data("kendoGrid").dataSource.read();
                             $("#grid").data('kendoGrid').refresh();
                         }
                     },
                     create: {
                         url: crudServiceBaseUrl + "/Create",
                         dataType: "json",
                         click: function (e) {
                             location.replace('/account/register')
                         },
                         complete: function (data) {
                             $("#grid").data("kendoGrid").dataSource.read();
                             $("#grid").data('kendoGrid').refresh();
                         }
                     },
                     parameterMap: function (options, operation) {
                         if (operation !== "read" && options.models) {
                             var str = JSON.stringify(options.models);
                             var lastStr = str.lastIndexOf("{");
                             str = str.substring(lastStr);
                             var tmp = "[";
                             str = tmp.concat(str);
                             return { models: str };
                         }
                     }
                 },
                 batch: true,
                 pageSize: 2,
                 schema: {
                     model: {
                         id: "Id",
                         fields: {
                             Id: { editable: false, nullable: false, hidden: true },
                             DepartmentID: { editable: true, nullable: false, hidden: true, validation: {  message: "Vui lòng nhập Phòng Ban!"  } },
                             DepartmentName: {  editable: false},
                             BranchID: { editable: true, nullable: false, hidden: true, validation: { required: { message: "Vui lòng nhập Chi Nhánh!" } } },
                             BranchName: { editable: false },
                             EmployeeName: { editable: false },
                             FirstName: { validation: { required: { message: "Vui lòng nhập tên!" } } },
                             MiddleName: {},
                             LastName: { validation: { required: { message: "Vui lòng nhập Họ!" } } },
                             Birthday: { type: "date", validation: { required: { message: "Vui lòng nhập Ngày Sinh!" } } },
                             Sex: { type: "boolean" },
                             Address: { validation: { required: { message: "Vui lòng nhập Địa Chỉ!" } } },
                             HireDate: { type: "date", validation: { required: { message: "Vui lòng nhập Ngày Vào Làm!" } } },
                             Salary: { type: "number", validation: { min: 0 } },
                             Email: { type: "email", validation: { required: { message: "Vui lòng nhập Email!" } } },
                             PhoneNumber: { type: "number" },
                             ImageEmployee: { },
                             
                               
                         }
                     }
                 }

             });

         $("#grid").kendoGrid({
             dataSource: dataSource,
             navigatable: true,
             //columnResizeHandleWidth: 6,
             allowCopy: true,
             pageable: true,
             groupable: true,
             //filterable: true,
             sortable: true,
             reorderable: true,
             resizable: true,
             // autoBind: false,
             selectable: true,
             columnMenu: true,
             height: 550,
             toolbar: [{ name: "create", text: "Thêm Nhân Viên Mới" }],
             edit: function(e) {
                
                     // hide the editor of the "id" column when editing data items
                 e.container.find("input[name=Picture]").parent().hide();
                 e.container.find("label[for=Picture]").parent().hide();
                 
                 e.container.find("input[name=Details]").parent().hide();
                 e.container.find("label[for=Details]").parent().hide();

                 e.container.find("label[for=DepartmentName]").parent().hide();
                 e.container.find("label[for=DepartmentName]").parent().hide();
                 
             },
             columns: [
                  
                 {
                     field: "DepartmentID", title: "Mã Phòng Ban", hidden:true,
                     editor: function (container) {
                         var input = $('<input id="Id" name="DepartmentID">');
                         // append to the editor container
                         input.appendTo(container);

                         // initialize a dropdownlist
                         input.kendoDropDownList({
                             dataTextField: "DepartmentName",
                             dataValueField: "DepartmentID",
                             dataSource: new kendo.data.DataSource({
                                 transport: {
                                     read: {
                                         url: crudServiceBaseUrl + "/GetgridDepartment",
                                         dateType: "json"
                                     },
                                     parameterMap: function (options, operation) {
                                         if (operation !== "read" && options.models) {
                                             var str = JSON.stringify(options.models);
                                             var lastStr = str.lastIndexOf("{");
                                             var str = str.substring(lastStr);
                                             var tmp = "[";
                                             str = tmp.concat(str);
                                             // alert(str);
                                             return { models: str };
                                         }
                                     }
                                 },
                                 schema: {
                                     model: {
                                         id: "DepartmentID",
                                         fields: {
                                             DepartmentID: {},
                                             DepartmentName: {},
                                         }
                                     }
                                 }
                             }) // bind it to the brands array
                         }).appendTo(container);
                     }
                 },
                 {
                     field: "BranchID", title: "Mã Chi Nhánh", hidden: true,
                     editor: function (container) {
                         var input = $('<input id="Id" name="BranchID">');
                         // append to the editor container
                         input.appendTo(container);

                         // initialize a dropdownlist
                         input.kendoDropDownList({
                             dataTextField: "BranchName",
                             dataValueField: "BranchID",
                             dataSource: new kendo.data.DataSource({
                                 transport: {
                                     read: {
                                         url: crudServiceBaseUrl + "/GetgridBranch",
                                         dateType: "json"
                                     },
                                     parameterMap: function (options, operation) {
                                         if (operation !== "read" && options.models) {
                                             var str = JSON.stringify(options.models);
                                             var lastStr = str.lastIndexOf("{");
                                             var str = str.substring(lastStr);
                                             var tmp = "[";
                                             str = tmp.concat(str);
                                             //alert(str);
                                             return { models: str };
                                         }
                                     }
                                 },
                                 schema: {
                                     model: {
                                         id: "BranchID",
                                         fields: {
                                             BranchID: {},
                                             BranchName: {},
                                         }
                                     }
                                 }
                             }) // bind it to the brands array
                         }).appendTo(container);
                     }
                 },
              {
                  field: "Picture", title: "<div class='text-center'><strong>Hình Ảnh</strong></div>", sortable: false, columnMenu: false, filterable: false, width: "166px", template: ' <img src="#:ImageEmployee#" style="width:150px; height:155px;" />'
              },
              { field: "Details", title: "<div class='text-center'><strong>Thông Tin</strong></div>", sortable: false, columnMenu: false,width: "343px", filterable: false, template: '<h3>#:EmployeeName#</h3><div>Giới Tính: #= Sex ? "Nam" : "Nữ" #</div><div>Ngày Sinh:#= kendo.toString(Birthday,"MM/dd/yyyy") #</div><div>Chi Nhánh: #:BranchName#</div> <div>Địa Chỉ: #:Address#</div><div>Số điện thoại: #:PhoneNumber#</div>' },
              { field: "DepartmentName", title: "Phòng Ban", width: "162px", template: '<label for="DepartmentName" class="text-center"><h2>#:DepartmentName#</h2></label>' },
                { field: "LastName", title: "Họ", width: "66px", hidden: true },
                 { field: "MiddleName", title: "Tên Lót", width: "74px", hidden: true },
                   { field: "FirstName", title: "Tên", width: "59px", hidden: true },
                 { field: "Birthday", title: "Ngày Sinh", width: "91px", template: '#= kendo.toString(Birthday,"MM/dd/yyyy") #',hidden:true },
                 { field: "Sex", title: "Giới Tính", width: "46px",hidden:true,template: "#= Sex ? 'Nam' : 'Nữ' #"},
                 { field: "Address", title: "Địa Chỉ", width: "180px",hidden:true },
                 { field: "HireDate", title: "Ngày Vào Làm", width: "138px", template: '#= kendo.toString(Birthday,"MM/dd/yyyy") #', hidden: false },
                 { field: "Salary", title: "Lương", width: "120px", hidden: true },
                 { field: "Email", title: "Email", width: "120px", hidden: true },
                 { field: "PhoneNumber", title: "SDT", width: "120px", hidden: true },
                
                    

                 {
                     command: [{
                         name: "edit",
                         text: { edit: "", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                     },
                     {
                         name: "destroy",
                         text: ""
                     },
                     {
                  name: "nguyencat", text: "Upload Ảnh",
                  click: function (e) {  //add a click event listener on the delete button
                      var tr = $(e.target).closest("tr"); //get the row for deletion
                      // alert(tr.toSource())
                      //console.log(tr);

                      try {
                          var data = this.dataItem(tr); //get the row data so it can be referred later
                          //alert(data.toSource())

                         
                          window.content(windowTemplate(data)); //send the row data object to the template and render it
                          window.open().center();
                          if (sessionStorage.initialFiles === undefined) {
                              sessionStorage.initialFiles = "[]";
                          }
                          var initialFiles = JSON.parse(sessionStorage.initialFiles);
                       
                          $("#files").kendoUpload({
                              multiple: false,
                              async: {
                                  saveUrl: "/account/upload?models="+data.Id,
                                  removeUrl: "/account/remove",
                                 // removeField: "customRemoveField",
                                  //removeVerb: "DELETE",
                                  //saveField: "customSaveField",
                                  //extension:"jpg",
                                  autoUpload: false
                              },
                              localization: {
                                  dropFilesHere: "Thả hình tại đây!",
                                  cancel: "Hủy bỏ",
                                  headerStatusUploaded: "Xong!",
                                  headerStatusUploading: "Đang Upload!",
                                  remove: "Xóa!",
                                  retry: "Thử lại!",
                                  select: "Nhập hình ảnh Nhân viên",
                                  statusFailed: "Thất bại!",
                                  statusUploaded: "Trạng thái Upload",
                                  uploadSelectedFiles: "CẬP NHẬT"
                              },
                              files: initialFiles,
                              success: onSuccess,
                              complete: onComplete,
                              select: onSelect
                          });
                          function onSelect(e) {
                           //   avatar = e.files[0].name;
                              
                          }
                          function onSuccess(e) {
                              alert("Cập Nhật Thành Công");
                              var currentInitialFiles = JSON.parse(sessionStorage.initialFiles);
                              for (var i = 0; i < e.files.length; i++) {
                                  var current = {
                                      name: e.files[i].name,
                                      extension: e.files[i].extension,
                                      size: e.files[i].size
                                  }

                                  if (e.operation == "upload") {
                                      currentInitialFiles.push(current);
                                  } else {
                                      var indexOfFile = currentInitialFiles.indexOf(current);
                                      currentInitialFiles.splice(indexOfFile, 1);
                                  }
                              }
                              sessionStorage.initialFiles = JSON.stringify(currentInitialFiles);
                          }
                          function onComplete(e) {
                              alert("Cập Nhật Thành Công");
                               location.reload(true);
                              
                              //$("#grid").data("kendoGrid").dataSource.read();
                              //$("#grid").data('kendoGrid').refresh();
                          }
                          
                      }
                      catch (e) {
                          //alert("Quý khách vui lòng chọn lại ngày cần thống kê"
                          //    + "\n Trang sẽ được tải lại trong giây lát!"
                          //    + "\n Xin cảm ơn và chúc quý khách một ngày làm việc vui vẻ! "
                          //    + "\n THEPHUONGSOLUTION COMPANY LIMITED ")
                      }
                      $("#yesButton").click(function () {
                        // LƯU UPLOAD
                          window.close();
                      })
                      $("#noButton").click(function () {
                          window.close();
                      })
                  }
              }
                     ], width: "250px"
                 }],
             editable: {
                 mode: "popup",
                 confirmation: function (e) {
                     return "Bạn có muốn xóa nhân viên " + e.EmployeeName + "?";
                 },
                 window: {
                     title: "Thêm/ Cập Nhật Nhân Viên",

                 }
                 // confirmation: true,
                 //cancelDelete: "No"
                 // confirmDelete: "Có"
             }
             // refresh:true
         });
         //var grid = $("#grid").data("kendoGrid");
     });
        
