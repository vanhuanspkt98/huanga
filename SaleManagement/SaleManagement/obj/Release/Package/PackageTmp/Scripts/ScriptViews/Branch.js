﻿$(document).ready(function () {
    var that = this;
    var crudServiceBaseUrl = "/Branch",
        dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/GetGrid",
                    dataType: "json"
                },
                update: {
                    url: crudServiceBaseUrl + "/Update",
                    dataType: "json",
                    complete: function (data) {
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: crudServiceBaseUrl + "/Destroy",
                    dataType: "json",
                    complete: function (data) {
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                create: {
                    url: crudServiceBaseUrl + "/Create",
                    dataType: "json",
                    complete: function (data) {
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                //aggregate: [
                // { field: "Price", aggregate: "sum" }
                //],
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        var str = JSON.stringify(options.models);
                        var lastStr = str.lastIndexOf("{");
                        var str = str.substring(lastStr);
                        var tmp = "[";
                        str = tmp.concat(str);

                        return { models: str };
                    }
                }
            },
            batch: true,
            pageSize: 20,
            schema: {
                model: {
                    id: "BranchID",
                    fields: {
                        BranchID: { editable: false, nullable: false, hidden: true },
                        StoreID: { editable: true, nullable: false, hidden: true },
                        // StoreName:{},
                        //  CategoryName:{editable:false},
                        BranchName: { validation: { required: true } },
                        BranchAddress: { validation: { required: true } },
                        PhoneNumber: { type: "number", validation: { required: true, min: 0 } },


                    }
                }
            }

        });

    $("#grid").kendoGrid({
        dataSource: dataSource,
        navigatable: true,
        selectable: true,
        //columnResizeHandleWidth: 6,
        allowCopy: true,
        pageable: true,
        groupable: true,
        sortable: true,
        reorderable: true,
        resizable: true,
        height: 550,
        toolbar: [{ name: "create", text: "Thêm Chi Nhánh Mới" }],
        columns: [
          //  { field: "BranchID", title: "Mã Chi Nhánh" },
            {
                field: "StoreName", title: "Tên Cửa Hàng", width: "120px",
                editor: function (container) {
                    var input = $('<input id="StoreID" name="StoreID">');
                    // append to the editor container
                    input.appendTo(container);

                    // initialize a dropdownlist
                    input.kendoDropDownList({
                        dataTextField: "StoreName",
                        dataValueField: "StoreID",
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: crudServiceBaseUrl + "/GetgridStore",
                                    dateType: "json"
                                },
                                parameterMap: function (options, operation) {
                                    if (operation !== "read" && options.models) {
                                        return { models: kendo.stringify(options.models) };
                                    }
                                }
                            },
                            schema: {
                                model: {
                                    id: "StoreID"
                                    //fields: {
                                    //    CategoryID: {},
                                    //    CategoryName: {},
                                    //}
                                }
                            }
                        }) // bind it to the brands array
                    }).appendTo(container);
                }
            },
            { field: "BranchName", title: "Tên Chi Nhánh", width: "120px" },
            { field: "BranchAddress", title: "Địa Chỉ", width: "120px" },
            { field: "PhoneNumber", title: "Số Điện Thoại", width: "120px" },

            {
                command: [{
                    name: "edit",
                    text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                },
                {
                    name: "destroy",
                    text: "Xóa"
                }
                ], title: "&nbsp;", width: "250px"
            }],
        editable: {
            mode: "popup",
            confirmation: function (e) {
                return "Bạn có muốn xóa Chi Nhánh " + e.BranchName + "?";
            },
            window: {
                title: "Thêm/ Cập Nhật Chi Nhánh",

            }
            // confirmation: true,
            //cancelDelete: "No"
            // confirmDelete: "Có"
        }
        // refresh:true
    });
});