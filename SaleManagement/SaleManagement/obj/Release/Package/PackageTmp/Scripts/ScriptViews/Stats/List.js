﻿
       $("#year").kendoDatePicker({
           start: "decade",
           depth: "decade",
           format: "yyyy"
       });

var date = new Date();
var grid = $('#grid').data('kendoGrid');
var value1 = date.getFullYear();
var db = new kendo.data.DataSource({
    type: "json",
    transport: {
        read: "/Stats/GetDataByYear?year="+value1
    },
    schema: {
        model: {
            fields: {
                Month: { type: "number" },
                Total: { type: "number" },
                SOPQ: { type: "number" },
                Date: { type: "string" }
            }
        }
    },
    pageSize: 7,
    group: {
        field: "Month",title:"Tháng" ,aggregates: [
              { field: "Date", aggregate: "count" },
              { field: "Total", aggregate: "sum" },
              { field: "SOPQ", aggregate: "sum" }
        ]
    },

    aggregate: [
          { field: "Month", aggregate: "count" },
          { field: "Date",  aggregate: "count" },
          { field: "Total", aggregate: "sum" },
          { field: "Total", aggregate: "min" },
          { field: "Total", aggregate: "max" },
          { field: "SOPQ", aggregate: "sum" }
    ]
}
);
$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: db,
        sortable: true,
        scrollable: false,
        pageable: true,
        columns: [
            {
                field: "Month", title: "Tháng",hidden:true
            },
               {
                   field: "Date", title: "Ngày Lập Hóa Đơn", aggregates: ["count"],
                   footerTemplate: "Tổng Cộng: #=count#",
                   groupFooterTemplate: "Tổng Cộng: #=count#"
               },
                {
                    field: "Total", title: "Tổng Tiền",format:"{0:c}", aggregates: ["sum", "min", "max"],
                    footerTemplate: "<div>Min: #= min #</div><div>Max: #= max #</div><div>Total: #= sum #</div>",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
                {
                    field: "SOPQ", title: "Số Lượng Sản Phẩm", aggregates: ["sum"],
                    footerTemplate: "Tổng Cộng: #=sum#",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                }



        ]
    });
});



    function selectyear() {
        var value1 = $('#year').val();
        db = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: "/Stats/GetDataByYear?year=" + value1
            },
            schema: {
                model: {
                    fields: {
                        Month: { type: "number" },
                        Total: { type: "number" },
                        SOPQ: { type: "number" },
                        Date: { type: "string" }
                    }
                }
            },
            pageSize: 7,
            group: {
                field: "Month", title: "Tháng", aggregates: [
                      { field: "Date", aggregate: "count" },
                      { field: "Total", aggregate: "sum" },
                      { field: "SOPQ", aggregate: "sum" }
                ]
            },

            aggregate: [
                  { field: "Month", aggregate: "count" },
                  { field: "Date", aggregate: "count" },
                  { field: "Total", aggregate: "sum" },
                  { field: "Total", aggregate: "min" },
                  { field: "Total", aggregate: "max" },
                  { field: "SOPQ", aggregate: "sum" }
            ]
        }
    );
        $("#grid").kendoGrid({
            dataSource: db,
            sortable: true,
            scrollable: false,
            pageable: true,
            columns: [
                {
                    field: "Month", title: "Tháng", hidden: true
                },
                   {
                       field: "Date", title: "Ngày Lập Hóa Đơn", aggregates: ["count"],
                       footerTemplate: "Tổng Cộng: #=count#",
                       groupFooterTemplate: "Tổng Cộng: #=count#"
                   },
                    {
                        field: "Total", title: "Tổng Tiền", format: "{0:c}", aggregates: ["sum", "min", "max"],
                        footerTemplate: "<div>Min: #= min #</div><div>Max: #= max #</div><div>Total: #= sum #</div>",
                        groupFooterTemplate: "Tổng Cộng: #=sum#"
                    },
                    {
                        field: "SOPQ", title: "Số Lượng Sản Phẩm", aggregates: ["sum"],
                        footerTemplate: "Tổng Cộng: #=sum#",
                        groupFooterTemplate: "Tổng Cộng: #=sum#"
                    }



            ]
        });
    }

