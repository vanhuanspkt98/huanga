﻿
     $(document).ready(function () {
         var that = this;
         var windowTemplate = kendo.template($("#windowTemplate").html());
         var crudServiceBaseUrl = "/Product",
             dataSource = new kendo.data.DataSource({
                 transport: {
                     read: {
                         url: crudServiceBaseUrl + "/GetGrid",
                         dataType: "json"
                     },
                     update: {
                         url: crudServiceBaseUrl + "/Update",
                         dataType: "json",
                         complete: function (data) {
                             $("#grid").data("kendoGrid").dataSource.read();
                             $("#grid").data('kendoGrid').refresh();
                         }
                     },
                     destroy: {
                         url: crudServiceBaseUrl + "/Destroy",
                         dataType: "json",
                         complete: function (e) {
                             $("#grid").data("kendoGrid").dataSource.read();
                             $("#grid").data('kendoGrid').refresh();
                         }
                     },
                     create: {
                         url: crudServiceBaseUrl + "/Create",
                         dataType: "json",
                         complete: function (data) {
                             $("#grid").data("kendoGrid").dataSource.read();
                             $("#grid").data('kendoGrid').refresh();
                         }
                     },
                     
                     //aggregate: [
                     // { field: "Price", aggregate: "sum" }
                     //],
                     parameterMap: function (options, operation) {
                         if (operation !== "read" && options.models) {
                             var str = JSON.stringify(options.models);
                             var lastStr = str.lastIndexOf("{");
                             var str = str.substring(lastStr);
                             var tmp = "[";
                             str = tmp.concat(str);

                             return { models: str };
                         }
                     }
                 },
                 batch: true,
                 pageSize: 10,
                 schema: {
                     model: {
                         id: "ProductID",
                         fields: {
                             ProductID: { editable: true, nullable: false, hidden: true, validation: { required: true } },
                             CategoryID: { editable: true, nullable: false, hidden: true, validation: { required: true } },
                             CategoryName: { editable: false },
                             ProductName: { validation: { required: true } },
                             Price: { type: "number", validation: { required: true, min: 1 } },
                             Discount: { type: "number", validation: { required: true, min: 0, max: 100 } },
                             Quantity: { type: "number", validation: { required: true, min: 0 } },
                             Tax: { type: "number", validation: { required: true, min: 0, max: 100 } }

                         }
                     }
                 }

             });

         var window = $("#window").kendoWindow({
             title: "In Mã Vạch",
             visible: false, //the window will not appear before its .open method is called
             width: "290px",
             height: "195px"
         }).data("kendoWindow");

         $("#grid").kendoGrid({
             dataSource: dataSource,
             navigatable: true,
             //columnResizeHandleWidth: 6,
             allowCopy: true,
             pageable: true,
             groupable: true,
             filterable: true,
             sortable: true,
             reorderable: true,
             resizable: true,
             // autoBind: false,
             selectable: true,
             columnMenu: true,
             height: 550,
             toolbar: [{ name: "create", text: "Thêm Sản Phẩm Mới" }],
             edit: function (e) {

                 // hide the editor of the "id" column when editing data items
                 e.container.find("input[name=CategoryName]").parent().hide();
                 e.container.find("label[for=CategoryName]").hide();

                 e.container.find("label[name=CategoryID]").parent().hide();

             },
             columns: [
                 { field: "ProductID", title: "Mã Sản Phẩm", filterable: false ,width:"90px"},
                 {
                     field: "CategoryID", title: "Mã Loại SP", hidden: true,
                     editor: function (container) {
                         var input = $('<input id="CategoryID" name="CategoryID">');
                         // append to the editor container
                         input.appendTo(container);

                         // initialize a dropdownlist
                         input.kendoDropDownList({
                             dataTextField: "CategoryName",
                             dataValueField: "CategoryID",
                             dataSource: new kendo.data.DataSource({
                                 transport: {
                                     read: {
                                         url: crudServiceBaseUrl + "/GetgridCategory",
                                         dateType: "json"
                                     },
                                     parameterMap: function (options, operation) {
                                         if (operation !== "read" && options.models) {
                                             var str = JSON.stringify(options.models);
                                             var lastStr = str.lastIndexOf("{");
                                             var str = str.substring(lastStr);
                                             var tmp = "[";
                                             str = tmp.concat(str);

                                             return { models: str };
                                         }
                                     }
                                 },
                                 schema: {
                                     model: {
                                         id: "CategoryID",
                                         fields: {
                                             CategoryID: {},
                                             CategoryName: {},
                                         }
                                     }
                                 }
                             }) // bind it to the brands array
                         }).appendTo(container);
                     }
                 },
                 { field: "CategoryName", title: "Loại Sản Phẩm", filterable: false ,width:"120px"},
                 { field: "ProductName", title: "Tên Sản Phẩm",width:"150px"},
                 { field: "Price", title: "Giá", format: "{0:c}", width: "120px", filterable: false },
                 { field: "Discount", title: "Khuyến Mãi", format: "{0:p0}", width: "80px", filterable: false },
                 { field: "Quantity", title: "Số Lượng", width: "80px", filterable: false },
                 { field: "Tax", title: "Thuế", format: "{0:p0}", width: "60px", filterable: false },

                 { width:"286",
                     command: [{
                         name: "edit",
                         text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                     },
                     {
                         name: "destroy",
                         text: "Xóa"
                     },
                     {
                         name: "printBarcode",
                         text: "In Mã Vạch",
                         click: function (e) {  //add a click event listener on the delete button
                             var tr = $(e.target).closest("tr"); //get the row for deletion
                             // alert(tr.toSource())
                             try {
                                 var data = this.dataItem(tr); //get the row data so it can be referred later
                                 //  alert(data.toSource())
                                 //console.log(data);
                                    
                                 window.content(windowTemplate(data)); //send the row data object to the template and render it
                                 window.open().center();
                                 $("#barcode").kendoBarcode({
                                     value: data.ProductID,
                                     type: "code128",
                                     width: 280,
                                     height: 100
                                 });
                                   
                             }
                             catch (e) {
                                 //alert("Quý khách vui lòng chọn lại ngày cần thống kê"
                                 //    + "\n Trang sẽ được tải lại trong giây lát!"
                                 //    + "\n Xin cảm ơn và chúc quý khách một ngày làm việc vui vẻ! "
                                 //    + "\n THEPHUONGSOLUTION COMPANY LIMITED ")
                             }

                             //$("#printButton").click(function () {
                             //    window.refresh();
                             //    window.open();
                             //    alert("");
                             //    window.close();
                             //})
                             $("#noButton").click(function () {
                                 window.close();
                             })
                         }
                     }
                     ], title: "&nbsp;", width: "250px"
                 }],
             editable: {
                 mode: "popup",
                 confirmation: function (e) {
                     return "Bạn có muốn xóa sản phẩm " + e.ProductName + "?";
                 },
                 window: {
                     title: "Thêm/ Cập Nhật Sản Phẩm",

                 }
                 // confirmation: true,
                 //cancelDelete: "No"
                 // confirmDelete: "Có"
             }
             // refresh:true
         });
     });
// search
(function ($, kendo) {

    // ID of the timeout "timer" created in the last key-press
    var timeout = 0;

    // Our search function 
    var performSearch = function () {

        // Our filter, an empty array mean "no filter"
        var filter = [];

        // Get the DataSource
        var dataSource = $('#grid').data('kendoGrid').dataSource;

        // Get and clean the search text.
        var searchText = $.trim($('#search').val());

        // Build the filter in case the user actually enter some text in the search field
        if (searchText) {

            // In this case I wanna make a multiple column search so the filter that I want to apply will be an array of filters, with an OR logic. 
            filter.push({
                logic: 'or',
                filters: [
                    { field: 'ProductID', operator: 'contains', value: searchText },
                    { field: 'ProductName', operator: 'contains', value: searchText }
                ]
            });
        }

        // Apply the filter.
        dataSource.filter(filter);
    };

    // Bind all the keyboard events that we wanna listen to the search field.
    $('#search').on('keyup, keypress, change, blur', function () {

        clearTimeout(timeout);
        timeout = setTimeout(performSearch, 250);
    });

})(window.jQuery, window.kendo);
function Test() {
    var fup = document.getElementById("upload");
    var fileName = fup.value;
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

    if (ext.toLowerCase() == "txt") {
        return true;
    }
    else {
        alert("File upload khong hop le");
        return false;
    }
}