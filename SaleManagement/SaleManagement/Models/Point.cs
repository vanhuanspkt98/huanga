﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
namespace SaleManagement.Models
{
    public class Point
    {
       
        public int PointID { get; set; }
        [DisplayName("Tỷ Lệ")]
        public int PointRatio { get; set; }
         [DisplayName("Giá trị quy đổi")]
        public int PointValue { get; set; }
    }
}