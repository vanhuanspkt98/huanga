﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class OrderDetailViewModel
    {
        [Key]
        
        public int OrderDetailID { get; set; }
        
        [DisplayName("Mã Hóa Đơn")]
        public int OrderID { get; set; }
        [DisplayName("Mã Sản Phẩm")]
        public string ProductID { get; set; }
        [DisplayName("Thành Tiền")]
        public double Price { get; set; }
        [DisplayName("Số Lượng")]
        public int Quantity { get; set; }
        //Product
        [DisplayName("Mã Loại Sản Phẩm")]
        public string CategoryID { get; set; }
        [DisplayName("Tên Sản Phẩm")]
        public string ProductName { get; set; }
        [DisplayName("Giá Sản Phẩm")]
        public double ProductPrice { get; set; }
        [DisplayName("Thuế")]
        public int ProductTax { get; set; }
        [DisplayName("Khuyến Mãi")]
        public int? ProductDiscount { get; set; }
        [DisplayName("Loại Sản Phẩm")]
        public string CategoryName { get; set; }

        public virtual OrderViewModel OrderViewModel { get; set; }
    }
}