﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SaleManagement.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FullName
        {
            get
            {
                return LastName + " " + MiddleName + " " + FirstName;
            }
        }
        [Required]
        [Display(Name = "Tên ")]
        public string FirstName { get; set; }
        [Display(Name = "Tên Đệm")]
        public string MiddleName { get; set; }
        [Required]
        [Display(Name = "Họ")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email ID")]
        [EmailAddress]
        public string EmailID { get; set; }
        //
        [DisplayName("Ngày Sinh")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Ngày Sinh!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
        ApplyFormatInEditMode = true)]
        public DateTime Birthday { get; set; }
        [DisplayName("Địa Chỉ")]
        [Required, StringLength(180)]
        public string Address { get; set; }
        [DisplayName("Giới Tính")]
        public bool Sex { get; set; }
        [Required(ErrorMessage = "Yêu Cầu Nhập Ngày Vào Làm!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
        ApplyFormatInEditMode = true)]
        [DisplayName("Ngày Vào Làm")]
        public DateTime HireDate { get; set; }

        [DisplayName("Lương")]
        // [Required(ErrorMessage = "Yêu Cầu Nhập Số Lượng!")]
        [Range(0, 100000000,
            ErrorMessage = "Nhập Sai Lương!")]
        public double Salary { get; set; }
        [DisplayName("Mã Phòng Ban")]
        public int DepartmentID { get; set; }
        [DisplayName("Hình Ảnh")]
        public string ImageEmployee { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public int BranchID { get; set; }
        public virtual Branch Branch { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class QLBHContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<IdentityUserRole> User_Roles { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<CustomerCategory> CustomerCategorys { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<ProviderDetail> ProviderDetails { get; set; }
        //view models
        public DbSet<OrderViewModel> OrderViewModels { get; set; }
        public DbSet<OrderDetailViewModel> OrderDetailViewModel { get; set; }
        public DbSet<Point> Points { get; set; }
        public QLBHContext()
            : base("QLBHContext", throwIfV1Schema: false)
        {
        }
        public static QLBHContext Create()
        {
            return new QLBHContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //tao cac khoa chinh cho cac bang
            modelBuilder.Entity<Branch>().HasKey(br => br.BranchID);
            modelBuilder.Entity<Customer>().HasKey(cus => cus.CustomerID);
            modelBuilder.Entity<CustomerCategory>().HasKey(c => c.CustomerCategoryID);
            modelBuilder.Entity<Department>().HasKey(dpm => dpm.DepartmentID);
            modelBuilder.Entity<Order>().HasKey(order => order.OrderID);
            modelBuilder.Entity<OrderDetail>().HasKey(cthd => cthd.OrderDetailID);
            modelBuilder.Entity<ProviderDetail>().HasKey(ordetail => ordetail.ProviderDetailID);
            modelBuilder.Entity<Product>().HasKey(pvd => pvd.ProductID);
            modelBuilder.Entity<Provider>().HasKey(prv => prv.ProviderID);
            modelBuilder.Entity<Store>().HasKey(store => store.StoreID);
            modelBuilder.Entity<Point>().HasKey(point => point.PointID);
            //view models
            modelBuilder.Entity<OrderDetailViewModel>().HasKey(s => s.OrderDetailID).Property(s => s.OrderDetailID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<OrderViewModel>().HasKey(s => s.OrderID).Property(s => s.OrderID).HasDatabaseGeneratedOption(databaseGeneratedOption: DatabaseGeneratedOption.Identity);
            //-----------Branch : Order---------------------------------------------------
            //modelBuilder.Entity<Order>()
            //    .HasRequired(Or => Or.Branch)
            //    .WithMany(Br => Br.Orders)
            //    .HasForeignKey(or => or.BranchID);

            modelBuilder.Entity<ApplicationUser>()
            .HasRequired(user => user.Branch)
            .WithMany(Br => Br.ApplicationUsers)
            .HasForeignKey(user => user.BranchID);

            //---------- Category: Product
            modelBuilder.Entity<Product>()
                .HasRequired(p => p.Category)
                .WithMany(cat => cat.Products)
                .HasForeignKey(pro => pro.CategoryID);
            //---------- CustomerCategory: Customer------------------------------------------
            modelBuilder.Entity<Customer>()
                .HasRequired(c => c.CustomerCategory)
                .WithMany(cl => cl.Customers)
                .HasForeignKey(c => c.CustomerCategoryID);
            //---------  Department: Employee------------------------------------------------
            modelBuilder.Entity<ApplicationUser>()
                .HasRequired(Em => Em.Department)
                .WithMany(dp => dp.Employees)
               .HasForeignKey(Em => Em.DepartmentID);
            //----------Employee: Order -------------------------------------------------------
            modelBuilder.Entity<Order>()
                .HasRequired(o => o.Employee)
                .WithMany(Em => Em.Orders)
                .HasForeignKey(or => or.EmployeeID);

            //-----------Order: OderDetail----------------------------------------------------
            modelBuilder.Entity<OrderDetail>()
               .HasRequired(od => od.Order)
               .WithMany(o => o.OrderDetails)
               .HasForeignKey(od => od.OrderID);
            //-----------Product OrderDetail, ProviderDetail-----------------------------------

            //--------- Provider: ProviderDetail-----------------------------------------------
            modelBuilder.Entity<ProviderDetail>()
                .HasRequired(pd => pd.Provider)
                .WithMany(p => p.ProviderDetails)
                .HasForeignKey(pd => pd.ProviderID);
            //--------- Store: Branch----------------------------------------------------------
            modelBuilder.Entity<Branch>()
                .HasRequired(br => br.Store)
                .WithMany(st => st.Branchs)
                .HasForeignKey(st => st.StoreID);
           // ------VIEW MODELS---------
            modelBuilder.Entity<OrderDetailViewModel>()
                .HasRequired(s => s.OrderViewModel)
                .WithMany(s => s.OrderDetailViewModels)
                .HasForeignKey(s => s.OrderID);
        }



        //  public System.Data.Entity.DbSet<SaleManagement.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}