﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class Branch
    {
        public int BranchID { get; set; }
        public int StoreID { get; set; }
        [Required(ErrorMessage = "Yêu Cầu Nhập Tên chi nhánh!")]
        [DisplayName("Tên Chi Nhánh")]
        public string BranchName { get; set; }
        [DisplayName("Tên địa chỉ chi nhánh")]
        [StringLength(380)]
        public string BranchAddress { get; set; }
        [DisplayName("Số điện thoại")]
        public string PhoneNumber { get; set; }
        public virtual Store Store { get; set; }
        //public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}