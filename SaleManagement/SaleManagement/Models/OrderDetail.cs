﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace SaleManagement.Models
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }
       // [Key, Column(Order = 1)]
        [DisplayName("Mã Hóa Đơn")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Mã Hóa Đơn!")]
        public int OrderID { get; set; }
       // [Key, Column(Order = 2)]
        [DisplayName("Mã Sản Phẩm")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Mã Sản Phẩm!")]
        public string ProductID { get; set; }
         [DisplayName("Thành Tiền")]
        public double Price { get; set; }
        [DisplayName("Số Lượng")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Số lượng!")]
        [Range(0, 10000000,
            ErrorMessage = "Nhập Sai Số Lượng!")]
        public int Quantity { get; set; }

       public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
    }
}