﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class ProviderDetail
    {
        public int ProviderDetailID { get; set; }
        [DisplayName("Mã Nhà Cung Cấp")]
        public int ProviderID { get; set; }
        [DisplayName("Mã Nhà Sản Phẩm")]
        public string  ProductID { get; set; }
        [DisplayName("Số Lượng")]
        public int Quantity { get; set; }
        [DisplayName("Giá Nhập Hàng")]
        public double Price { get; set; }

        public virtual Product Product { get; set; }
        public virtual Provider Provider { get; set; }
    }
}