﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class Category
    {
        [Key]
        [DisplayName("Mã Loại Sản Phẩm")]
        [Required(ErrorMessage = "Yêu Cầu Mã Loại Sản Phẩm!")]
        public string CategoryID { get; set; }
        [DisplayName("Loại Sản Phẩm")]
        [Required(ErrorMessage = "Yêu Cầu Tên Loại Sản Phẩm!")]
        public string CategoryName { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}