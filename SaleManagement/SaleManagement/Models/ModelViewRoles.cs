﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class ModelViewRoles
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public bool IsAccountant { get; set; }
        public bool IsStorekeeper { get; set; }
    }
}