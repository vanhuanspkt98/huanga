﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class Provider
    {
        [DisplayName("Mã Nhà Cung Cấp")]
        public int ProviderID { get; set; }
        [DisplayName("Tên Nhà Cung Cấp")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Tên Nhà Cung Cấp!")]
        public string ProviderName { get; set; }
        [StringLength(380)]
        [DisplayName("Địa Chỉ")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Địa Chỉ Nhà Cung Cấp!")]
        public string ProviderAddress { get; set; }
        [DisplayName("Số Điện Thoại")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Số Điện Thoại Nhà Cung Cấp!")]
        public string PhoneNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        public virtual ICollection<ProviderDetail> ProviderDetails { get; set; }
    }
}