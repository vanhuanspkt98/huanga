﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class OrderViewModel
    {
        [Key]
        //public int ID { get; set; }
        [DisplayName("Mã Hóa Đơn")]
        public int OrderID { get; set; }
      
        [DisplayName("Tổng Tiền")]
        public double Total { get; set; }
        [DisplayName("Tiền Khách Trả")]
        public double PrepaymentAmount { get; set; }
        [DisplayName("Tổng Số Lượng")]
        public int Quantity { get; set; }
        [DisplayName("Tổng Tiền Khuyến Mãi")]
        public double Discount { get; set; }
        [DisplayName("Ngày Lập Hóa Đơn")]
        public DateTime DateOfOrder { get; set; }
        //Employee
        [DisplayName("Mã Nhân Viên")]
        public string EmployeeID { get; set; }
        [Display(Name = "Tên ")]
        public string EmployeeName { get; set; }
        //Department
        [DisplayName("Mã Phòng Ban")]
        public int DepartmentID { get; set; }
        [DisplayName("Phòng Ban")]
        public string DepartmentName { get; set; }
        //Branch
        [DisplayName("Mã Chi Nhánh")]
        public int BranchID { get; set; }
        [DisplayName("Tên Chi Nhánh")]
        public string BranchName { get; set; }
        [DisplayName("Tên địa chỉ chi nhánh")]
        public string BranchAddress { get; set; }
        //Store
        public int StoreID { get; set; }
        [DisplayName("Tên Công Ty")]
        public string StoreName { get; set; }
        [DisplayName("Địa Chỉ")]
        public string StoreAddress { get; set; }
        //customer
        public int? CustomerID { get; set; }
        public string CustomerName { get; set; }
       public virtual ICollection<OrderDetailViewModel> OrderDetailViewModels { get; set; }

    }
}