﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace SaleManagement.Models
{
    public class Order
    {
      
       // [Key]
       // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Mã Hóa Đơn")]
        public int OrderID { get; set; }
        [DisplayName("Mã Khách Hàng")]
        public int? CustomerID { get; set; }
        [DisplayName("Mã Nhân Viên")]
        public string EmployeeID { get; set; }
        [DisplayName("Mã Công Ty")]
        public int BranchID { get; set; }
        [DisplayName("Tổng Tiền")]
        public double Total { get; set; }
        [DisplayName("Tiền Khách Trả")]
        public double PrepaymentAmount { get; set; }
        [DisplayName("Tổng Số Lượng")]
        public int Quantity { get; set; }
        [DisplayName("Tổng Tiền Khuyến Mãi")]
        public double Discount { get; set; }
        [DisplayName("Ngày Lập Hóa Đơn")]
        public DateTime  DateOfOrder{ get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual  ApplicationUser Employee { get; set; }
        //public virtual Branch Branch { get; set; }
    }
}