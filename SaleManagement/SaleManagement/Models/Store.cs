﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleManagement.Models
{
    public class Store
    {
        public int StoreID { get; set; }
        [DisplayName("Tên Công Ty")]
        public string StoreName { get; set; }
        [DisplayName("Địa Chỉ")]
        [StringLength(380)]
        public string StoreAddress { get; set; }
        [DisplayName("Số điện thoại")]
        public string PhoneNumber { get; set; }
        [DisplayName("Website")]
        public string Website { get; set; }
        public virtual ICollection<Branch> Branchs { get; set; }
    }
}