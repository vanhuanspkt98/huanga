﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
   using System.ComponentModel.DataAnnotations.Schema;
namespace SaleManagement.Models
{
    public class Department
    {
        [DisplayName("Mã Phòng Ban")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Mã Phòng Ban!")]
        public int DepartmentID { get; set; }
        [DisplayName("Phòng Ban")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Tên Phòng Ban!")]
        public string DepartmentName { get; set; }
        public String ManagerID { get; set; }
        public virtual ICollection<ApplicationUser> Employees { get; set; }
    }
}