﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace SaleManagement.Models
{
    public class CustomerCategory
    {
        [DisplayName("Mã Loại Khách Hàng")]
        [Required(ErrorMessage = "Yêu Cầu Mã Loại Khách Hàng!")]
        public string CustomerCategoryID { get; set; }
        [DisplayName("Loại Khách Hàng")]
        [Required(ErrorMessage = "Yêu Cầu Tên Loại Khách Hàng!")]
        public string CustomerCategoryName { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}