﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace SaleManagement.Models
{
    public class Customer
    {
        [DisplayName("Mã Khách Hàng")]
        public int CustomerID { get; set; }
        //public string Username { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [DisplayName("Họ")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Họ!")]
        public string LastName { get; set; }
        [DisplayName("Tên Lót")]
        public string MiddleName { get; set; }
        [Display(Name = "Họ và Tên")]
        public string FullName
        {
            get
            {
                return LastName + " " + MiddleName + " " + FirstName;
            }
        }
        [Required(ErrorMessage = "Yêu Cầu Nhập Tên!")]
        [DisplayName("Tên")]
        public string FirstName { get; set; }
        [DisplayName("Ngày Sinh")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Ngày Sinh!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
        ApplyFormatInEditMode = true)]
        public DateTime Birthday { get; set; }
        [DisplayName("Giới Tính")]
        public string Sex { get; set; }
        [DisplayName("Địa Chỉ")]
        [StringLength(380)]
        public string Address { get; set; }
        [DisplayName("Số Điện Thoại")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Số Điện Thoại!")]
        // [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4||5})$"
        //  ,ErrorMessage = "Số Điện Thoại Không Hợp Lệ!")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Yêu Cầu Mã Loại Khách Hàng!")]
        [DisplayName("Mã Loại Khách Hàng")]
        public string CustomerCategoryID { get; set; }
        [DisplayName("Điểm Tích Lũy")]
        public int? Point { get; set; }

        public virtual CustomerCategory CustomerCategory { get; set; }
    }
}