﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
    using Microsoft.AspNet.Identity.EntityFramework;
namespace SaleManagement.Models
{
    public class Product
    {    
        [DisplayName("Mã Sản Phẩm")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Mã Sản Phẩm!")]
        public string ProductID { get; set; }

        [DisplayName("Mã Loại Sản Phẩm")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Mã Loại Sản Phẩm!")]
        public string CategoryID { get; set; }

        [DisplayName("Tên Sản Phẩm")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Tên Sản Phẩm!")]
        public string ProductName { get; set; }


      //  [Required(ErrorMessage = "Yêu Cầu Nhập Mã Nhà Sản Xuất!")]
       // public int MaNSX { get; set; }
        
        [DisplayName("Đơn Giá")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Đơn Giá!")]
        public double Price { get; set; }

        [DisplayName("Số Lượng")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Số Lượng!")]
        [Range(-1000000, 1000000,
           ErrorMessage = "Nhập Sai Số Lượng!")]
        public int Quantity { get; set; }

        [DisplayName("Thuế")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Thuế!")]
        [Range(0, 100)]
        public int Tax { get; set; }

        [DisplayName("Khuyến Mãi")]
        public int? Discount { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<ProviderDetail> ProviderDetails { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
      
    }
}