﻿

    function enterAsTab() {
        var keyPressed = event.keyCode; // get the Key that is pressed
        if (keyPressed == 13) {
            //case the KeyPressed is the [Enter]
            var inputs = $('input'); // storage a array of Inputs
            var a = inputs.index(document.activeElement);
            //get the Index of Active Element Input inside the Inputs(array)

            
            var ProductID = $("#ProductID").val();
            $.getJSON("/bill/CheckProductID", { ProductID: ProductID }, function (DataSource, StatusText, Xhr) {
                if (DataSource.infor == 'true') {
                    var Quantity = $("#" + ProductID).val();
                    if (Quantity != null) {
                        Quantity = parseInt(Quantity);
                        Quantity += 1;
                    }
                    else {
                        Quantity = 1;
                    }
                    //Kiem tra tinh hop le cua du lieu
                    var result = ValiQuantity(Quantity);
                    if (result != 'true') {
                        alert(result);
                    } else {
                        $("#tongtien").remove();
                        $.getJSON("/bill/CheckQuantity", { ProductID: ProductID, Quantity: Quantity }, function (DataSource, StatusText, Xhr) {
                            if (DataSource.infor == 'ok') {
                                // alert(DataSource.infor);
                                $.getJSON("/bill/AddToCard", { ProductID: ProductID, Quantity: Quantity }, ReloadView);
                                $("#ProductID").val('');
                                $("#ProductID").focus();
                                // $("#txtSoLuong").val(0);
                            }
                            else {
                                alert("Không còn đủ số lượng, hiện tại còn: " + DataSource.infor);
                            }
                        });
                    }
                }
                else {
                    alert('Mã sản phẩm không tồn tại');
                }
            });
           // alert(1);
            if (inputs[a + 1] !== null) {
                // case the next index of array is not null
                var nextBox = inputs[a + 1];
                nextBox.focus(); // Focus the next input element. Make him an Active Element
                event.preventDefault();
            }
           // alert(2);
            return false;
        }

        else { return keyPressed; }
    }



    var id;
$(document).ready(function (eOuter) {
    $('input').bind('keypress', function (eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            tabindex++; //increment tabindex
            $('[tabindex=' + tabindex + ']').focus();

          //  $('#Msg').text($(this).attr('id') + " tabindex: " + tabindex + " next element: " + $('*').attr('tabindex').id);


            // to cancel out Onenter page postback in asp.net
            return false;
        }
    });
});

var dbCustomer = new kendo.data.DataSource({
    transport: {
        read: {
            url: '/Bill/GetCustomer',
            dataType: "json"
        }
    }
});
var dbProduct = new kendo.data.DataSource({
    transport: {
        read: {
            url: '/Bill/GetMaSP',
            dataType: "json"
        }
    }
});

    //  $("#txtSoLuong").kendoNumericTextBox();

$(document).ready(function () {
  
    $("#ProductID").kendoAutoComplete({
        dataSource: dbProduct,
        dataTextField: "ProductID",
        dataValueField: "ProductID",
        filter: "contains",
        placeholder: "Chọn Sản phẩm...",
    });
    //create AutoComplete UI component
    $("#CustomerID").kendoAutoComplete({
        dataSource: dbCustomer,
        filter: "contains",
        dataTextField: "CustomerID",
        //dataValueField: "CustomerID",
        headerTemplate: '<div class="header-temp">' +
                              '<span class="header-temp-span">' +
                                   'Mã KH' +
                              '</span>' +
                              '<span class=".col-xs-6">' +
                                   'Thông Tin' +
                              '</span>' +
                          '</div>',

        template:
            '<table>' +
                '<tr>' +
                    '<td class="header-temp-test">' +
                        '<span class="k-state-default">#:CustomerID#</span>' +
                    '</td>' +
                    '<td>' +
                        '<table>' +
                            '<tr>' +
                                '<td>' +
                                    '<span><b>#:CustomerName#</b></span>' +
                                '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>' +
                                    '<span>#:PhoneNumber#</span>' +
                                '</td>' +
                            '</tr>' +
                        '</table>' +
                    '</td>' +
                '</tr>' +
            '</table>',
    });
});
