﻿  $(document).ready(function () {
             var that = this;
             var crudServiceBaseUrl = "/ProviderDetail",
                 dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: crudServiceBaseUrl + "/GetGrid",
                             dataType: "json"
                         },
                         update: {
                             url: crudServiceBaseUrl + "/Update",
                             dataType: "json",
                             complete: function (data) {
                                 $("#grid").data("kendoGrid").dataSource.read();
                             }
                         },
                         destroy: {
                             url: crudServiceBaseUrl + "/Destroy",
                             dataType: "json",
                             complete: function (data) {
                                 $("#grid").data("kendoGrid").dataSource.read();
                             }
                         },
                         create: {
                             url: crudServiceBaseUrl + "/Create",
                             dataType: "json",
                             complete: function (data) {
                                 $("#grid").data("kendoGrid").dataSource.read();
                             }
                         },
                         //aggregate: [
                         // { field: "Price", aggregate: "sum" }
                         //],
                         parameterMap: function (options, operation) {
                             if (operation !== "read" && options.models) {
                                 var str = JSON.stringify(options.models);
                                 var lastStr = str.lastIndexOf("{");
                                 var str = str.substring(lastStr);
                                 var tmp = "[";
                                 str = tmp.concat(str);
                                     
                                 return { models: str };
                             }
                         }
                     },
                     batch: true,
                     pageSize: 20,
                     schema: {
                         model: {
                             id: "ProviderDetailID",
                             fields: {
                                 ProviderDetailID: { editable: false, nullable: false, hidden: true },
                                 ProviderID: { editable: true },
                                 ProductID: { editable: true },
                                 Quantity: { type: "number", validation: { required: true, min: 0 } },
                                 Price: { type: "number", validation: { required: true, min: 0 } },
                             }
                         }
                     }

                 });

             $("#grid").kendoGrid({
                 dataSource: dataSource,
                 navigatable: true,
                 selectable: true,
                 //columnResizeHandleWidth: 6,
                 allowCopy: true,
                 pageable: true,
                 groupable: true,
                 sortable: true,
                 reorderable: true,
                 resizable: true,
                 height: 550,
                 toolbar: [{ name: "create", text: "Thêm Chi Tiết Nhà Cung Cấp Mới"}],
                 columns: [
                  //   { field: "ProviderDetailID", title: "Mã Chi Tiết Nhà Cung Cấp" },
                     { field: "ProviderID", title: "Mã Nhà Cung Cấp", width: "120px" },
                      { field: "ProductID", title: "Mã Sản Phẩm", width: "120px" },
                      { field: "Quantity", title:"Số Lượng",width:"120px" },
                     { field: "Price", title: "Giá", width: "120px" },
                     {
                         command: [{
                             name: "edit",
                             text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                         },
                         {
                             name: "destroy",
                             text: "Xóa"
                         }
                         ], title: "&nbsp;", width: "250px"
                     }],
                 editable: {
                     mode: "popup",
                     confirmation: function (e) {
                         return "Bạn có muốn xóa Nhà Cung Cấp " + e.ProviderDetailName + "?";
                     },
                     window: {
                         title: "Thêm/ Cập Nhật Nhà Cung Cấp",

                     }
                     // confirmation: true,
                     //cancelDelete: "No"
                     // confirmDelete: "Có"
                 }
                 // refresh:true
             });
         });