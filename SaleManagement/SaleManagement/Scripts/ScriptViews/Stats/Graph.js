﻿
            $(document).ready(function () {
                function startChange() {
                    var startDate = start.value(),
                    endDate = end.value();

                    if (startDate) {
                        startDate = new Date(startDate);
                        startDate.setDate(startDate.getDate());
                        end.min(startDate);
                    } else if (endDate) {
                        start.max(new Date(endDate));
                    } else {
                        endDate = new Date();
                        start.max(endDate);
                        end.min(endDate);
                    }
                }

                function endChange() {
                    var endDate = end.value(),
                    startDate = start.value();

                    if (endDate) {
                        endDate = new Date(endDate);
                        endDate.setDate(endDate.getDate());
                        start.max(endDate);
                    } else if (startDate) {
                        end.min(new Date(startDate));
                    } else {
                        endDate = new Date();
                        start.max(endDate);
                        end.min(endDate);
                    }
                }

                var start = $("#start").kendoDatePicker({
                    change: startChange
                }).data("kendoDatePicker");

                var end = $("#end").kendoDatePicker({
                    change: endChange
                }).data("kendoDatePicker");

                start.max(end.value());
                end.min(start.value());
            });
$("#day").kendoDatePicker({
    start: "month",
    depth: "month",
    format: "MM/dd/yyyy"
});
$("#month").kendoDatePicker({
    start: "year",
    depth: "year",
    format: "MM/yyyy"
});
$("#year").kendoDatePicker({
    start: "decade",
    depth: "decade",
    format: "yyyy"
});

            function load() {
                var chart = $("#chart").data("kendoChart");
                chart.dataSource.read();
                chart.refresh();
            };

    $('#refresh').click(function () {
        var chart = $("#chart").data("kendoChart");
        chart.dataSource.read();
        chart.refresh();
    });

    var date = new Date();
$("#chart").kendoChart({
    dataSource: {
        transport: {
            read: {
                url: "/Stats/GraphDTByYear?year=" + date.getFullYear(),
                dataType: "json"
            }
        }
    },
    title: {
        text: "Biểu Đồ Thống Kê Doanh Thu Trong Năm "
    },
    legend: {
        visible: true,
        position: "top"
    },
    series: [{
        name: "Doanh Thu",
        data: "dataSource",
        type: "column",
        // style: "smooth",
        field: "Turnover",
        color: "rgb(60,141,188)",
        gap: 0.2,
        spacing: 0.2,
        markers: {
            visible: false
        }
    }
    ],
    categoryAxis: {
        type: "month",
        categories: [" Một", " Hai", " Ba", " Tư", " Năm", " Sáu", " Bảy", " Tám", " Chín", " Mười", " Mười Một", " Mười Hai"],
        plotBands: [{
            //from: new Date("2015/12/31"),
            //to: new Date("2015/01/01"),
            // color: "green"
        }],
        //select: {
        //    from: 0,
        //    to:2
        //}
    },
    tooltip: {
        visible: true,
        format: "{0}%",
        template: "#= series.name #: #= value #"
    }
});

    function GraphOrderYear() {
       
        var value1 = $('#year').val();
        // var value2 = $('styleChart').val();
        $("#chart").kendoChart({
            dataSource: {
                transport: {
                    read: {
                        url: "/Stats/GraphDTbyYear?year=" + value1,
                        dataType: "json"
                    }
                }
            },
            title: {
                text: "Biểu Đồ Thống Kê Doanh Thu Theo Năm"
            },
            legend: {
                visible: true,
                position: "top"
            },
            seriesDefault: {
                type: "column",
                style: "smooth",
                stack: true
            },
            series: [{
                name: "Doanh Thu",
                color: "green",
                type: "column",
                field: "Turnover",
                gap: 0.2,
                spacing: 0.2,
                //  categories:"Time",
                markers: {
                    visible: false
                }
            }],
            categoryAxis: {
                categories: [" Một", " Hai", " Ba", " Tư", " Năm", " Sáu", " Bảy", " Tám", " Chín", " Mười", " Mười Một", " Mười Hai"],
                majorGridLines: {
                    visible: false
                },
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: false
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    function GraphOrderMonth() {
        var value1 = $('#month').val();

        $("#chart").kendoChart({
            dataSource: {
                transport: {
                    read: {
                        url: "/Stats/GraphDTByMonth?month=" + value1,
                        dataType: "json"
                    }
                }
            },
            title: {
                text: "Biểu Đồ Thống Kê Doanh Thu Theo Tháng"
            },
            legend: {
                visible: true,
                position: "top"
            },
            seriesDefault: {
                type: "area",
                style: "smooth",
                stack: true
            },
            series: [{
                name: "Doanh Thu",
                color: "green",
                type: "area",
                field: "Turnover",
                //  categories:"Time",
                markers: {
                    visible: false
                }
            }],
            categoryAxis: {
                categories: ["1", "2", "3", "4","5", "6", "7", "8", "9", "10", "11", " 12", "13", "14", "15", "16","17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                majorGridLines: {
                    visible: false
                },
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: false
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }

        });
    }
    function GraphOrderDay() {
                var value1 = $('#day').val();

                $("#chart").kendoChart({
                    dataSource: {
                        transport: {
                            read: {
                                url: "/Stats/GraphDTByDay?day=" + value1,
                                dataType: "json"
                            }
                        }
                    },
                    title: {
                        text: "Biểu Đồ Thống Kê Doanh Thu Theo Ngày"
                    },
                    legend: {
                        visible: true,
                        position: "top"
                    },
                    seriesDefault: {
                        type: "line",
                        style: "smooth",
                        stack: true
                    },
                    series: [{
                        name: "Doanh Thu",
                        color: "green",
                        type: "line",
                        field: "Turnover",
                        //  categories:"Time",
                        markers: {
                            visible: false
                        }
                    }],
                    categoryAxis: {
                        categories: ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
                        majorGridLines: {
                            visible: false
                        },
                        majorGridLines: {
                            visible: false
                        },
                        majorTicks: {
                            visible: false
                        }
                    },
                    tooltip: {
                        visible: true,
                        format: "{0}%",
                        template: "#= series.name #: #= value #"
                    }

                });
            }
    function GraphDaybyDay() {
        var from = $('#start').val();
        var to = $('#end').val();

        $("#chart").kendoChart({
            dataSource: {
                transport: {
                    read: {
                        url: "/Stats/GraphDTDaybyDay?from=" + from + "&&to=" + to,
                        dataType: "json"
                    }
                }
            },
            title: {
                text: "Biểu Đồ Thống Kê Doanh Thu "
            },
            legend: {
                visible: true,
                position: "top"
            },
            seriesDefault: {
                type: "column",
                style: "smooth",
                stack: true
            },
            series: [{
                name: "Doanh Thu",
                color: "green",
                type: "area",
                field: "Turnover",
                //  categories:"Time",
                markers: {
                    visible: false
                }
            }],
            categoryAxis: {
                // categories: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", " 12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                majorGridLines: {
                    visible: false
                },
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: false
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }

        });
    }
