﻿
    $(document).ready(function () {
        var windowTemplate = kendo.template($("#windowTemplate").html());
        dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/Stats/GetCustomer",
                    dataType: "json"
                },
                update: {
                    url: "",
                    dataType: "json"
                },
                destroy: {
                    url: "",
                    dataType: "json"
                },
                create: {
                    url: "",
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        var str = JSON.stringify(options.models);
                        var lastStr = str.lastIndexOf("{");
                        var str = str.substring(lastStr);
                        var tmp = "[";
                        str = tmp.concat(str);

                        return { models: str };
                    }
                }
            },
            batch: true,
            pageSize: 7,
            schema: {
                model: {
                    id: "CustomerID",
                    fields: {
                        CustomerID: { type: "string" },
                        CustomerName: { type: "string" },
                        Total: { type: "number" },
                        Quantity: { type: "number" },
                        Birthday: { type: "date" },
                        Sex: { type: "boolean" },
                        Point: { type: "number" }
                    }
                }
            },
            //group: {
            //    field: "CustomerID", aggregates: [
            //         { field: "OrderID", aggregate: "count" },
            //         { field: "Quantity", aggregate: "sum" },
            //         { field: "Total", aggregate: "sum" },
            //         { field: "Point", aggregate: "sum" }
            //    ]
            //},

            //aggregate: [
            //     // { field: "CustomerID", aggregate: "count" },
            //      { field: "OrderID", aggregate: "count" },
            //      { field: "Quantity", aggregate: "sum" },
            //      { field: "Point", aggregate: "sum" },
            //      { field: "Total", aggregate: "sum" }, { field: "Total", aggregate: "min" }, { field: "Total", aggregate: "max" }
            //]

        });

        var window = $("#window").kendoWindow({
            title: "",
            visible: false, //the window will not appear before its .open method is called
            width: "400px",
            height: "340px",
        }).data("kendoWindow");


        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            sortable:true,
            //  groupable:true,
            //  filterable:true,
            height: 430,
            // toolbar: ["create"],
            columns: [
                 {
                     field: "CustomerID", title: "Mã Khách Hàng", hidden: false
                 }, {
                     field: "CustomerName", title: "Khách Hàng"
                 },
                {
                    field: "Quantity", title: "Số Lượng Sản Phẩm",
                    //      footerTemplate: "Tổng Cộng: #=sum#",
                    //groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
                   {
                       field: "Total", title: "Tổng Tiền", format: "{0:c}",
                       //  footerTemplate: "<div>Nhỏ nhất: #= min #</div><div>Lớn Nhất: #= max #</div><div>Tổng:  #= sum #</div>",
                       // groupFooterTemplate: "Tổng Cộng: #=sum#"
                   }, {
                       field: "Point", title: "Điểm Tích Lũy",
                       //   footerTemplate: "Tổng Cộng: #=sum#",
                       // groupFooterTemplate: "Tổng Cộng: #=sum#"
                   },

            {
                command: [
                  {
                      name: "Delete", text: "CHI TIẾT",
                      click: function (e) {  //add a click event listener on the delete button
                          var tr = $(e.target).closest("tr"); //get the row for deletion
                          // alert(tr.toSource())


                          try {
                              var data = this.dataItem(tr); //get the row data so it can be referred later
                            
                              window.content(windowTemplate(data)); //send the row data object to the template and render it
                              window.open().center();

                          }
                          catch (e) {
                              //alert("Quý khách vui lòng chọn lại ngày cần thống kê"
                              //    + "\n Trang sẽ được tải lại trong giây lát!"
                              //    + "\n Xin cảm ơn và chúc quý khách một ngày làm việc vui vẻ! "
                              //    + "\n THEPHUONGSOLUTION COMPANY LIMITED ")
                          }

                          $("#noButton").click(function () {
                              window.close();
                          })
                      }
                  }
                ]
            }],
            editable: {
                mode: "popup"

            }
        }).data("kendoGrid");
    });
function onSearch() {
    var q = $("#txtSearchString").val();
    $("#grid").data('kendoGrid').dataSource.query({
        page: 1,
        pageSize: 7,
        filter: {
            logic: "or",
            filters: [
              { field: "CustomerName", operator: "contains", value: q },
              { field: "CustomerID", operator: "contains", value: q }
            ]
        }
    });

    $("#grid").data('kendoGrid').dataSource.read();
    $("#grid").data('kendoGrid').refresh();

}

