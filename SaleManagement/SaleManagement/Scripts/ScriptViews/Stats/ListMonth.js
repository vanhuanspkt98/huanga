﻿
    $("#month").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MM yyyy"
    });


    var date = new Date();
var value1=date.getUTCMonth()+1 + "/" + date.getFullYear();
$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                read: "/Stats/GetDataByMonth?month="+value1
            },
            schema: {
                model: {
                    fields: {
                        //  Month: { type: "number" },
                        Total: { type: "number" },
                        SOPQ: { type: "number" },
                        Time: { type: "string" }
                    }
                }
            },
            pageSize: 7,
            //group: {
            //    field: "Date" ,aggregates: [
            //          { field: "Month", aggregate: "count" },
            //          { field: "Total", aggregate: "sum" },
            //          { field: "SOPQ", aggregate: "sum" }
            //    ]
            //},

            aggregate: [
                 // { field: "Month", aggregate: "count" },
                  { field: "Time",  aggregate: "count" },
                  { field: "Total", aggregate: "sum" },
                  { field: "Total", aggregate: "min" },
                  { field: "Total", aggregate: "max" },
                  { field: "SOPQ", aggregate: "sum" }
            ]
        },
        sortable: true,
        scrollable: false,
        pageable: true,
        columns: [
               {
                   field: "Time", title: "Ngày Lập Hóa Đơn", aggregates: ["count"],
                   footerTemplate: "Tổng Cộng: #=count#",
                   groupFooterTemplate: "Tổng Cộng: #=count#"
               },
                {
                    field: "Total", title: "Tổng Tiền",format:"{0:c}", aggregates: ["sum", "min", "max"],
                    footerTemplate: "<div>Nhỏ nhất: #= min #</div><div>Lớn nhất: #= max #</div><div>Tổng cộng: #= sum #</div>",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                },
                {
                    field: "SOPQ", title: "Số Lượng Sản Phẩm", aggregates: ["sum"],
                    footerTemplate: "Tổng Cộng: #=sum#",
                    groupFooterTemplate: "Tổng Cộng: #=sum#"
                }



        ]
    });
});



    function test() {
        var value1 = $('#month').val();
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: "/Stats/GetDataByMonth?month=" + value1
                    },
                    schema: {
                        model: {
                            fields: {
                                //  Month: { type: "number" },
                                Total: { type: "number" },
                                SOPQ: { type: "number" },
                                Time: { type: "string" }
                            }
                        }
                    },
                    pageSize: 7,
                    //group: {
                    //    field: "Month", title: "Tháng", aggregates: [
                    //          { field: "Date", aggregate: "count" },
                    //          { field: "Total", aggregate: "sum" },
                    //          { field: "SOPQ", aggregate: "sum" }
                    //    ]
                    //},

                    aggregate: [
                        //  { field: "Month", aggregate: "count" },
                          { field: "Time", aggregate: "count" },
                          { field: "Total", aggregate: "sum" },
                          { field: "Total", aggregate: "min" },
                          { field: "Total", aggregate: "max" },
                          { field: "SOPQ", aggregate: "sum" }
                    ]
                },
                sortable: true,
                scrollable: false,
                pageable: true,
                columns: [
                       {
                           field: "Time", title: "Ngày Lập Hóa Đơn", aggregates: ["count"],
                           footerTemplate: "Tổng Cộng: #=count#",
                           groupFooterTemplate: "Tổng Cộng: #=count#"
                       },
                        {
                            field: "Total", title: "Tổng Tiền", format: "{0:c}", aggregates: ["sum", "min", "max"],
                            footerTemplate: "<div>Nhỏ nhất: #= min #</div><div>Lớn nhất: #= max #</div><div>Tổng cộng: #= sum #</div>",
                            groupFooterTemplate: "Tổng Cộng: #=sum#"
                        },
                        {
                            field: "SOPQ", title: "Số Lượng Sản Phẩm", aggregates: ["sum"],
                            footerTemplate: "Tổng Cộng: #=sum#",
                            groupFooterTemplate: "Tổng Cộng: #=sum#"
                        }



                ]
            });
        });
    }
