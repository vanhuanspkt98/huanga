﻿
          $(document).ready(function () {
              var that = this;
              var crudServiceBaseUrl = "/Department",
                  dataSource = new kendo.data.DataSource({
                      transport: {
                          read: {
                              url: crudServiceBaseUrl + "/GetGrid",
                              dataType: "json"
                          },
                          update: {
                              url: crudServiceBaseUrl + "/Update",
                              dataType: "json",
                              complete: function (data) {
                                  $("#grid").data("kendoGrid").dataSource.read();
                              }
                          },
                          destroy: {
                              url: crudServiceBaseUrl + "/Destroy",
                              dataType: "json",
                              complete: function (data) {
                                  $("#grid").data("kendoGrid").dataSource.read();
                              }
                          },
                          create: {
                              url: crudServiceBaseUrl + "/Create",
                              dataType: "json",
                              complete: function (data) {
                                  $("#grid").data("kendoGrid").dataSource.read();
                              }
                          },
                          //aggregate: [
                          // { field: "Price", aggregate: "sum" }
                          //],
                          parameterMap: function (options, operation) {
                              if (operation !== "read" && options.models) {
                                  var str = JSON.stringify(options.models);
                                  var lastStr = str.lastIndexOf("{");
                                  var str = str.substring(lastStr);
                                  var tmp = "[";
                                  str = tmp.concat(str);
                                     
                                  return { models: str };
                              }
                          }
                      },
                      batch: true,
                      pageSize: 20,
                      schema: {
                          model: {
                              id: "DepartmentID",
                              fields: {
                                  DepartmentID: { editable: false, nullable: false, hidden: true },
                                  DepartmentName: { editable: true },
                                  ManagerID: {  },
                              }
                          }
                      }

                  });

              $("#grid").kendoGrid({
                  dataSource: dataSource,
                  navigatable: true,
                  selectable: true,
                  //columnResizeHandleWidth: 6,
                  allowCopy: true,
                  pageable: true,
                  groupable: true,
                  sortable: true,
                  reorderable: true,
                  resizable: true,
                  height: 550,
                  toolbar: [{ name: "create", text: "Thêm Phòng Ban Mới"}],
                  columns: [
                     // { field: "DepartmentID", title: "Mã Phòng Ban" },
                      { field: "DepartmentName", title: "Tên Phòng Ban", width: "120px" },
                      { field: "ManagerID", title: "Mã Quản Lý", width: "120px" },
                      {
                          command: [{
                              name: "edit",
                              text: { edit: "Chỉnh Sửa", update: "Cập Nhật", cancel: "Hủy Bỏ" }
                          },
                          {
                              name: "destroy",
                              text: "Xóa"
                          }
                          ], title: "&nbsp;", width: "250px"
                      }],
                  editable: {
                      mode: "popup",
                      confirmation: function (e) {
                          return "Bạn có muốn xóa Phòng Ban " + e.DepartmentName + "?";
                      },
                      window: {
                          title: "Thêm/ Cập Nhật Phòng Ban",

                      }
                      // confirmation: true,
                      //cancelDelete: "No"
                      // confirmDelete: "Có"
                  }
                  // refresh:true
              });
          });
