﻿//Print hóa đơn
function printContent(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
}
(function ($) {
    $('.spinner .btn:first-of-type').on('click', function () {
        $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 1);
    });
    $('.spinner .btn:last-of-type').on('click', function () {
        $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1);
    });
})(jQuery);
function ValiQuantity(value) {
    if (!isNaN(value)) {
        if (value > 0) return 'true';
        else {
            return 'Số lượng phải > 0';
        }
    } else {
        return "Số lượng nhập vào không hợp lệ";
    }
}
function PageOnload() {
    $.getJSON("/bill/getShoppingCardItems", {}, ReloadView);
}
function ReloadView2(DataSource, StatusText, Xhr) {
    var paren = $("#laphoadon");
    var tbody = paren.children("tbody").text("");
}
function ReloadView(DataSource, StatusText, Xhr) {
    var paren = $("#laphoadon");
    var tbody = paren.children("tbody").text("");
    var Total = 0;
    var sum = 0;
    var result;
    for (var i = 0; i < DataSource.length; i++) {
        var temp = DataSource[i].ProductID;
        var test =
         '<tr>' +
           '<th class="k-label text-center">' + (i + 1) + '</th>' +
           '<th class="k-label text-center">' + DataSource[i].ProductName + '</th>' +
           '<th><input  class="k-textbox  text-center" value=' + DataSource[i].Quantity + ' id="' + temp + '" /></th>' +
           '<th>' + DataSource[i].Price + '₫</th>' +
           '<th>' +
                '<button class="k-button btn-danger" onclick="btnXoa_OnClick(' + "&quot;" + temp + '&quot;)">&nbsp;&nbsp;Xóa&nbsp;&nbsp;</button>&nbsp;' +
                '<button class="k-button" onclick="btnUpdateQuality_OnClick(' + "&quot;" + temp + '&quot;)">Cập nhật SL</button>' +
          '</th>' +
          '</tr>';
        Total = Total + DataSource[i].Price;
        result = result + test;
    }
    if (DataSource.length > 0) {
        result = result + 'tr>' +
            '<td colspan="5"><br/></td></tr>' +
            '<tr><th>Tổng tiền: </th><th id="TongTien" colspan="3" class="k-label">' + Total + '₫</th>' +
        '</tr>' +
    '<tr >' +
     '<td colspan="5" style="text-align:center"s>' +
     '<button type="button" class="k-button k-primary"" onclick="btnXuaHD_Onclick()">Xuất Hóa Đơn</button>' +
     '&nbsp;' +
     '<button type="button" class="btn-danger k-button " onclick="HuyHoaDon()">Hủy Hóa Đơn</button></th>' +
     '</tr>';
    }
    $('#laphoadon').append(result);
}
function HuyHoaDon() {
    var result = window.confirm("Bạn muốn hủy hóa đơn ?");
    if (result == true) {
        $.get("/Bill/CancelOrder", {}, ReloadView2);
    }
}
function btnXuaHD_Onclick() {
    window.location = "/bill/Preview";
}
//function CheckProductID(){
//    var result=
//    }
//Khi btnThem click
var Quantity = 0;
function btnThem_Onclick() {
    var ProductID = $("#ProductID").val();
    $.getJSON("/bill/CheckProductID", { ProductID: ProductID }, function (DataSource, StatusText, Xhr) {
        if (DataSource.infor == 'true') {
            var Quantity = $("#" + ProductID).val();
            if (Quantity != null) {
                Quantity = parseInt(Quantity);
                Quantity += 1;
            }
            else {
                Quantity = 1;
            }
            //Kiem tra tinh hop le cua du lieu
            var result = ValiQuantity(Quantity);
            if (result != 'true') {
                alert(result);
            } else {
                $("#tongtien").remove();
                $.getJSON("/bill/CheckQuantity", { ProductID: ProductID, Quantity: Quantity }, function (DataSource, StatusText, Xhr) {
                    if (DataSource.infor == 'ok') {
                        // alert(DataSource.infor);
                        $.getJSON("/bill/AddToCard", { ProductID: ProductID, Quantity: Quantity }, ReloadView);
                        $("#ProductID").val('');
                        $("#ProductID").focus();
                       // $("#txtSoLuong").val(0);
                    }
                    else {
                        alert("Không còn đủ số lượng, hiện tại còn: " + DataSource.infor);
                    }
                });
            }
        }
        else {
            alert('Mã sản phẩm không tồn tại');
        }
    });
}
//xuat thong tin khach hang
function infoKH() {
    //var tmp = $("#CustomerID").val();
    //if (tmp != null)
    //    var CustomerID = parseInt(tmp);
    //$.getJSON("/bill/checkCustomerID", { CustomerID: CustomerID }, function (DataSource, StatusText, Xhr) {
    //    var paren = $("#info");
    //    if (DataSource.info == "ok") {
    //        var tbody = paren.children("tbody").text("");
    //        localStorage.setItem("list", JSON.stringify(DataSource));
    //        console.log(DataSource);
    //    } else {
    //        localStorage.clear("list");
    //    }
    //    var test =
    //'<tr><td>' +
    //CustomerID
    //    + '</td>'
    //    '</tr>';
        //if (DataSource.infor.info == 'ok') {
        //    $("#info").append(test);
        //}
        //else {
        //    $("#info").append(test);
        //}
  //ss  });
}
//khi btn Xoa di mot mat hang nao do
function btnXoa_OnClick(ProductID) {
    $("#tongtien").remove();
     Quantity = 0;
    $.getJSON("/bill/RemoveShoppingCardItem", { ProductID: ProductID }, ReloadView);
}
//khi btn Cap nhat lai so luong OnClick
function btnUpdateQuality_OnClick(ProductID) {
    var Quantity = $("#" + ProductID).val();
    var result = ValiQuantity(Quantity);
    if (result != 'true') {
        alert(result);
    } else {
        $.getJSON("/bill/CheckQuantity", { ProductID: ProductID, Quantity: Quantity }, function (DataSource, StatusText, Xhr) {
            if (DataSource.infor == 'ok') {
                alert("Cập nhật số lượng thành công!");
                $("#tongtien").remove();
                $.getJSON("/bill/AddToCard", { ProductID: ProductID, Quantity: Quantity }, ReloadView);
            }
            else {
                alert("Không còn đủ số lượng, hiện tại còn: " + DataSource.infor);
            }
        });
    }
}
