namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addi8 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.OrderDetailViewModel");
            AddPrimaryKey("dbo.OrderDetailViewModel", "OrderDetailID");
            DropColumn("dbo.OrderDetailViewModel", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderDetailViewModel", "ID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.OrderDetailViewModel");
            AddPrimaryKey("dbo.OrderDetailViewModel", "ID");
        }
    }
}
