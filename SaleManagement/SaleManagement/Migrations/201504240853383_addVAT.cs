namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addVAT : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "VAT", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "VAT");
        }
    }
}
