namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addid3 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.OrderViewModel");
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderViewModel", "ID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderViewModel", "ID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.OrderViewModel");
            AlterColumn("dbo.OrderViewModel", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderViewModel", "OrderID");
        }
    }
}
