namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderViewModel", "ID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderViewModel", "ID");
        }
    }
}
