namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeBranch_Ỏder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Order", "BranchID", "dbo.Branch");
            DropIndex("dbo.Order", new[] { "BranchID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Order", "BranchID");
            AddForeignKey("dbo.Order", "BranchID", "dbo.Branch", "BranchID", cascadeDelete: true);
        }
    }
}
