namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPoint : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Point",
                c => new
                    {
                        PointID = c.Int(nullable: false, identity: true),
                        PointValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PointID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Point");
        }
    }
}
