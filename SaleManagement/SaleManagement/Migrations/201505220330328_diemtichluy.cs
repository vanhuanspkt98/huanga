namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class diemtichluy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customer", "Point", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customer", "Point");
        }
    }
}
