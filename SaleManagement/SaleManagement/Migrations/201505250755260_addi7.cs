namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addi7 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.OrderDetailViewModel");
            AlterColumn("dbo.OrderDetailViewModel", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderDetailViewModel", "OrderDetailID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderDetailViewModel", "ID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.OrderDetailViewModel");
            AlterColumn("dbo.OrderDetailViewModel", "OrderDetailID", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderDetailViewModel", "ID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderDetailViewModel", "ID");
        }
    }
}
