namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pointratio : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Point", "PointRatio", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Point", "PointRatio");
        }
    }
}
