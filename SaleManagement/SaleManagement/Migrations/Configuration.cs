﻿namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using SaleManagement.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    internal sealed class Configuration : DbMigrationsConfiguration<QLBHContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        bool AddUserAndRole(QLBHContext context)
        {
            IdentityResult ir;
            var rm = new RoleManager<IdentityRole>
                (new RoleStore<IdentityRole>(context));
            //Tạo 3 quyền cơ bản
            ir = rm.Create(new IdentityRole("Admin"));
            ir = rm.Create(new IdentityRole("Accountant"));
            ir = rm.Create(new IdentityRole("Storekeeper"));
            //tạo một tài khoản duy nhất admin
            var um = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser()
            {
                UserName = "admin@tps.com",
                Address = "TP.HCM",
                Birthday = DateTime.Now,
                Email = "admin@tps.com",
                EmailConfirmed = true,
                EmailID = "admin@tps.com",
                FirstName = "Viên",
                LastName = "Quản Trị",
                Salary = 1,
                Sex = true,
                HireDate = DateTime.Now,
                DepartmentID = 1,
                BranchID = 1
            };
            ir = um.Create(user, "P_assw0rd1");

            if (ir.Succeeded == false)
                return ir.Succeeded;
            ir = um.AddToRole(user.Id, "Admin");
            return ir.Succeeded;
        }
        protected override void Seed(SaleManagement.Models.QLBHContext context)
        {
            context.Stores.AddOrUpdate(new Store() { 
                StoreID=1,
                StoreName="Thế Phương Solution",
                StoreAddress="Thành phố Hồ Chí Minh",
                PhoneNumber="084088888",
                Website="http://thephuongsolution.com"
            });
            context.Branchs.AddOrUpdate(new Branch() { 
                BranchID=1,
                StoreID=1,
                BranchName="TPS Thủ Đức",
                BranchAddress="Thủ Đức",
                PhoneNumber="098888888"
            });
            context.Departments.AddOrUpdate(new Department()
            {
                DepartmentID=1,
                DepartmentName="TPS Office",
                ManagerID=""
            });
            context.Points.AddOrUpdate(new Point()
            {
                PointID = 1,
                PointValue = 10000,
                PointRatio=1
            });
            context.CustomerCategorys.AddOrUpdate(new CustomerCategory() { 
                CustomerCategoryID="KL",
                CustomerCategoryName="Khách Lẻ"
            });
            context.Customers.AddOrUpdate(new Customer() { 
                CustomerID=1,
                Email="customer@tps.com",
                LastName="Khách",
                FirstName="Lẻ",
                Birthday=DateTime.Parse("1/1/2015"),
                Sex="Nữ",
                Address="TPS Company",
                PhoneNumber="0888888888",
                CustomerCategoryID="KL",
                Point=0
            });
            //var ls = context.Users.Select(u => u).ToList();
            //context.Departments.Add(new Department()
            //{
            //    DepartmentID = 1,
            //    DepartmentName = "Nhân Sự",
            //    ManagerID = ""
            //});
            //var ls = context.Users.Select(u => u).ToList();
            //foreach (var user in ls)
            //{

            //    user.BranchID = 1;
            //}
            //context.Users.AddOrUpdate(ls);
            //foreach (var user in context.Users.Select(u => u))
            //{

            //    user.BranchID = 13;
            //}
            //context.SaveChanges();
          //  var deID = context.Departments.First().DepartmentID;
            AddUserAndRole(context);
        }
    }
}
