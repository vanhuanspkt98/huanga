namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updating : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel");
            DropPrimaryKey("dbo.OrderDetailViewModel");
            DropPrimaryKey("dbo.OrderViewModel");
            AlterColumn("dbo.OrderDetailViewModel", "OrderDetailID", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.OrderDetailViewModel", "OrderDetailID");
            AddPrimaryKey("dbo.OrderViewModel", "OrderID");
            AddForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel", "OrderID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel");
            DropPrimaryKey("dbo.OrderViewModel");
            DropPrimaryKey("dbo.OrderDetailViewModel");
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.OrderDetailViewModel", "OrderDetailID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderViewModel", "OrderID");
            AddPrimaryKey("dbo.OrderDetailViewModel", "OrderDetailID");
            AddForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel", "OrderID", cascadeDelete: true);
        }
    }
}
