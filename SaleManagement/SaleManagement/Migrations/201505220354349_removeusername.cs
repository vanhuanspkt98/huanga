namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeusername : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Customer", "Username");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customer", "Username", c => c.String());
        }
    }
}
