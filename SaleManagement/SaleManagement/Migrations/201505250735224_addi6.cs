namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addi6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel");
            DropPrimaryKey("dbo.OrderViewModel");
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderViewModel", "OrderID");
            CreateIndex("dbo.OrderDetailViewModel", "OrderID");
            AddForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel", "OrderID", cascadeDelete: true);
            DropColumn("dbo.OrderViewModel", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderViewModel", "ID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel");
            DropIndex("dbo.OrderDetailViewModel", new[] { "OrderID" });
            DropPrimaryKey("dbo.OrderViewModel");
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.OrderViewModel", "ID");
            AddForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel", "OrderID", cascadeDelete: true);
        }
    }
}
