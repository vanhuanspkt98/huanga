namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addid2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel");
            DropIndex("dbo.OrderDetailViewModel", new[] { "OrderID" });
            DropPrimaryKey("dbo.OrderDetailViewModel");
            DropPrimaryKey("dbo.OrderViewModel");
            AlterColumn("dbo.OrderDetailViewModel", "OrderDetailID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.OrderDetailViewModel", "OrderDetailID");
            AddPrimaryKey("dbo.OrderViewModel", "OrderID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.OrderViewModel");
            DropPrimaryKey("dbo.OrderDetailViewModel");
            AlterColumn("dbo.OrderViewModel", "OrderID", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderDetailViewModel", "OrderDetailID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.OrderViewModel", "OrderID");
            AddPrimaryKey("dbo.OrderDetailViewModel", "OrderDetailID");
            CreateIndex("dbo.OrderDetailViewModel", "OrderID");
            AddForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel", "OrderID", cascadeDelete: true);
        }
    }
}
