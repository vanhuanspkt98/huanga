namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class akfmlksdgl : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProviderDetail", "ProductID", "dbo.Product");
            DropIndex("dbo.ProviderDetail", new[] { "ProductID" });
            AddColumn("dbo.Product", "Tax", c => c.Int(nullable: false));
            AlterColumn("dbo.ProviderDetail", "ProductID", c => c.String(maxLength: 128));
            CreateIndex("dbo.ProviderDetail", "ProductID");
            AddForeignKey("dbo.ProviderDetail", "ProductID", "dbo.Product", "ProductID");
            DropColumn("dbo.Product", "VAT");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Product", "VAT", c => c.Double(nullable: false));
            DropForeignKey("dbo.ProviderDetail", "ProductID", "dbo.Product");
            DropIndex("dbo.ProviderDetail", new[] { "ProductID" });
            AlterColumn("dbo.ProviderDetail", "ProductID", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Product", "Tax");
            CreateIndex("dbo.ProviderDetail", "ProductID");
            AddForeignKey("dbo.ProviderDetail", "ProductID", "dbo.Product", "ProductID", cascadeDelete: true);
        }
    }
}
