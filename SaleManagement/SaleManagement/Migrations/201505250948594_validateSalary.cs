namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class validateSalary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "HireDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AspNetUsers", "Address", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Customer", "Address", c => c.String(maxLength: 80));
            DropColumn("dbo.AspNetUsers", "StartingDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "StartingDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customer", "Address", c => c.String());
            AlterColumn("dbo.AspNetUsers", "Address", c => c.String());
            DropColumn("dbo.AspNetUsers", "HireDate");
        }
    }
}
