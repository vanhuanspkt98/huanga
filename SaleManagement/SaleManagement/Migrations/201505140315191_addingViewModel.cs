namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingViewModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderDetailViewModel",
                c => new
                    {
                        OrderDetailID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        ProductID = c.String(),
                        Price = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CategoryID = c.String(),
                        ProductName = c.String(),
                        ProductPrice = c.Double(nullable: false),
                        ProductTax = c.Int(nullable: false),
                        ProductDiscount = c.Int(),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.OrderDetailID)
                .ForeignKey("dbo.OrderViewModel", t => t.OrderID, cascadeDelete: true)
                .Index(t => t.OrderID);
            
            CreateTable(
                "dbo.OrderViewModel",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        Total = c.Double(nullable: false),
                        PrepaymentAmount = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Discount = c.Double(nullable: false),
                        DateOfOrder = c.DateTime(nullable: false),
                        EmployeeID = c.String(),
                        EmployeeName = c.String(),
                        DepartmentID = c.Int(nullable: false),
                        DepartmentName = c.String(),
                        BranchID = c.Int(nullable: false),
                        BranchName = c.String(),
                        BranchAddress = c.String(),
                        StoreID = c.Int(nullable: false),
                        StoreName = c.String(),
                        StoreAddress = c.String(),
                        CustomerID = c.Int(),
                        CustomerName = c.String(),
                    })
                .PrimaryKey(t => t.OrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetailViewModel", "OrderID", "dbo.OrderViewModel");
            DropIndex("dbo.OrderDetailViewModel", new[] { "OrderID" });
            DropTable("dbo.OrderViewModel");
            DropTable("dbo.OrderDetailViewModel");
        }
    }
}
