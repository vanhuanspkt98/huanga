namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addid4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetailViewModel", "ID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetailViewModel", "ID");
        }
    }
}
