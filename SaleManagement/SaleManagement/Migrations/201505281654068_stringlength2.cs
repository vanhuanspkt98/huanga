namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stringlength2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Branch", "BranchAddress", c => c.String(maxLength: 380));
            AlterColumn("dbo.Provider", "ProviderAddress", c => c.String(nullable: false, maxLength: 380));
            AlterColumn("dbo.Store", "StoreAddress", c => c.String(maxLength: 380));
            AlterColumn("dbo.Customer", "Address", c => c.String(maxLength: 380));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customer", "Address", c => c.String(maxLength: 180));
            AlterColumn("dbo.Store", "StoreAddress", c => c.String(maxLength: 180));
            AlterColumn("dbo.Provider", "ProviderAddress", c => c.String(nullable: false, maxLength: 180));
            AlterColumn("dbo.Branch", "BranchAddress", c => c.String(maxLength: 180));
        }
    }
}
