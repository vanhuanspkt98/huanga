namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stringlength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Branch", "BranchAddress", c => c.String(maxLength: 180));
            AlterColumn("dbo.Provider", "ProviderAddress", c => c.String(nullable: false, maxLength: 180));
            AlterColumn("dbo.Store", "StoreAddress", c => c.String(maxLength: 180));
            AlterColumn("dbo.Customer", "Address", c => c.String(maxLength: 180));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customer", "Address", c => c.String(maxLength: 100));
            AlterColumn("dbo.Store", "StoreAddress", c => c.String(maxLength: 80));
            AlterColumn("dbo.Provider", "ProviderAddress", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Branch", "BranchAddress", c => c.String(maxLength: 80));
        }
    }
}
