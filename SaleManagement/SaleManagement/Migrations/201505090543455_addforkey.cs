namespace SaleManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addforkey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.AspNetUsers", "BranchID");
            AddForeignKey("dbo.AspNetUsers", "BranchID", "dbo.Branch", "BranchID", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "BranchID", "dbo.Branch");
            DropIndex("dbo.AspNetUsers", new[] { "BranchID" });
        }
    }
}
