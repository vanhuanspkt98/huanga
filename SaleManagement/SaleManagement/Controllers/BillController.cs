﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
namespace SaleManagement.Controllers
{
    [Authorize(Roles = "Accountant,Admin")]
    public class ShoppingCardItem
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
        //khuyen mai
        public double Discount
        {
            get
            {
                return (Math.Round(double.Parse(((Quantity * Product.Price) * Product.Discount * 0.01).ToString())));
            }
        }
        //thue
        public double Tax
        {
            get
            {
                return (Math.Round(double.Parse(((Product.Price * Quantity
                    * Product.Tax * 0.01).ToString()))));
            }
        }
        public double TotalValue
        {
            get { return (Quantity * Product.Price); }
        }
        public double Value
        {
            get { return (Quantity * Product.Price) + Tax - Discount; }
        }
        public double Prepay { get; set; }

    }
    [Authorize(Roles = "Accountant,Admin")]
    public class ShoppingCard
    {
        public int? CustomerID;
        private List<ShoppingCardItem> CardItems = new List<ShoppingCardItem>();
        public List<ShoppingCardItem> Items
        {
            get { return CardItems; }
        }
        public double TotalValue
        {
            get { return Items.Sum(i => i.Value); }
        }

        public double TotalValue2()
        {
            return Items.Sum(i => i.TotalValue);
        }

        public void AddItem(string ProductID, int quantity)
        {
            ShoppingCardItem item = Items.Where(i => i.Product.ProductID.ToLower() == ProductID.ToLower()).FirstOrDefault();
            if (item == null)
            {
                QLBHContext db = new QLBHContext();
                var p = db.Products.Single(p1 => p1.ProductID.ToLower() == ProductID.ToLower());
                ShoppingCardItem shopItem = new ShoppingCardItem()
                {
                    Product = p,
                    Quantity = quantity
                };
                CardItems.Add(shopItem);
            }
            else
            {
                item.Quantity += quantity - item.Quantity;
            }
        }
        public void RemoveItem(string ProductID)
        {
            ShoppingCardItem item = Items.Single(i => i.Product.ProductID == ProductID);
            Items.Remove(item);

        }
        public void Clear()
        {
            CardItems.Clear();
        }
    }


    public class BillController : Controller
    {
        private static QLBHContext db = new QLBHContext();
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult CheckQuantity(int Quantity, string ProductID)
        {
            var pro = db.Products.Find(ProductID);
            if (pro.Quantity >= Quantity)
            {
                var infor = new { infor = "ok" };
                return Json(infor, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var infor = new { infor = pro.Quantity };
                return Json(infor, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult CancelOrder()
        {
            getShopingCard().Clear();
            string result = "{result:true}";
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Accountant,Admin")]
        public ShoppingCard getShopingCard()
        {
            ShoppingCard shopCard = (ShoppingCard)Session["shoppingcard"];
            if (shopCard == null)
            {
                shopCard = new ShoppingCard();
                Session["shoppingcard"] = shopCard;
            }
            return shopCard;
        }
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult AddToCard(string ProductID, int Quantity)
        {
            getShopingCard().AddItem(ProductID, Quantity);
            return Json(getShopingCard().Items.Select(s => new { ProductName = s.Product.ProductName, ProductID = s.Product.ProductID, Quantity = s.Quantity, Price = s.Value }).ToList(), JsonRequestBehavior.AllowGet);



        }
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult getShoppingCardItems()
        {
            return Json(getShopingCard().Items.Select(s => new { ProductName = s.Product.ProductName, ProductID = s.Product.ProductID, Quantity = s.Quantity, Price = s.Value }).ToList(), JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult RemoveShoppingCardItem(string ProductID)
        {
            ShoppingCard shop = getShopingCard();
            shop.RemoveItem(ProductID);
            return Json(getShopingCard().Items.Select(s => new { ProductName = s.Product.ProductName, ProductID = s.Product.ProductID, Quantity = s.Quantity, Price = s.Value }).ToList(), JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult CheckProductID(string ProductID)
        {
            var pro = db.Products.Where(p => p.ProductID.ToLower() == ProductID.ToLower()).Select(p => p).FirstOrDefault();
            if (pro != null)
            {
                var infor = new { infor = "true" };
                return Json(infor, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var infor = new { infor = "Invalidate" };
                return Json(infor, JsonRequestBehavior.AllowGet);
            }
        }
         [Authorize(Roles = "Accountant,Admin")]
        public ActionResult CheckCustomerID(int? CustomerID)
        {
            var shopping = getShopingCard();
            var lst = db.Customers.Where(s => s.CustomerID == CustomerID).Select(s => s).FirstOrDefault();
            if (lst != null)
            {
                shopping.CustomerID = CustomerID; ;
                var infor = new { info = "ok", CustomerID = lst.CustomerID, CustomerName = lst.LastName + " " + lst.MiddleName + " " + lst.FirstName, PhoneNumber = lst.PhoneNumber , Birthday=lst.Birthday,Sex= lst.Sex};
                return Json(infor, JsonRequestBehavior.AllowGet);
            }
            else
            {
                shopping.CustomerID = 1;
                var infor = new { info = "Khách lẻ" };
                return Json(infor, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize(Roles = "Accountant,Admin")]
        
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult Create()
        {
            return View();
        }
        //GET
        [Authorize(Roles = "Accountant,Admin")]
        public ActionResult Preview()
        {
            db = new QLBHContext();
            Order hd = new Order();
            OrderViewModel hdView = new OrderViewModel();
            var lstSession = (ShoppingCard)Session["shoppingcard"];
            if (lstSession != null)
            {
                OrderDetail cthd = new OrderDetail();
                OrderDetailViewModel cthdView = new OrderDetailViewModel();
                //HOA DON
                // add Order
                var maxOrder = db.Orders.Select(s => s.OrderID);
                if (maxOrder.Count() == 0)
                {
                    hd.OrderID = 1;
                }
                else
                {
                    hd.OrderID = maxOrder.Max() + 1;
                }
                hd.CustomerID = lstSession.CustomerID ;
                if (hd.CustomerID == null)
                    hd.CustomerID = 1;
                var kh = db.Customers.Find(hd.CustomerID);

                if (kh != null)
                {
                    Point point = new Point();
                    point = db.Points.First();
                    kh.Point += (int)lstSession.TotalValue /( point.PointValue/point.PointRatio);
                }
                
                Session["OrderID"] = hd.OrderID;
                var Employee = db.Users.Find(User.Identity.GetUserId());
                //adding order
                hd.BranchID = Employee.BranchID;
                hd.DateOfOrder = DateTime.Now;
                hd.EmployeeID = Employee.Id;
                db.Orders.Add(hd);
                //adding order view
                db.SaveChanges();
                //
                foreach (var item in lstSession.Items)
                {
                    cthd = new OrderDetail();
                    cthd.OrderID = hd.OrderID;
                    cthd.ProductID = item.Product.ProductID;
                    cthd.Quantity = item.Quantity;
                    //price for every product
                    cthd.Price = item.Value;
                    db.OrderDetails.Add(cthd);
                }
                db.SaveChanges();
                //cap nhat lai thong tin chi tiet qua hoa don
                //Tổng số lượng
                hd.Quantity = lstSession.Items.Select(s => s.Quantity).Sum();
                // tổng tiền khuyến mãi
                hd.Discount = lstSession.Items.Select(s => s.Discount).Sum();
                //Tổng tiền - bao gồm khuyến mãi
                hd.Total = lstSession.TotalValue;

               
                //Trừ số lượng sản phẩm đã mua
                Product sp;
                foreach (var item in lstSession.Items)
                {
                    sp = new Product();
                    sp = db.Products.Find(item.Product.ProductID);
                    sp.Quantity -= item.Quantity;

                }
                db.SaveChanges();

                //view order

                
                hdView.OrderID = hd.OrderID;
                hdView.CustomerID = hd.CustomerID;
                hdView.DateOfOrder = DateTime.Now;
                hdView.EmployeeID = Employee.Id;
                hdView.EmployeeName = Employee.FullName;
                hdView.DepartmentID = Employee.DepartmentID;
                hdView.DepartmentName = Employee.Department.DepartmentName;
                hdView.BranchID = Employee.BranchID;
                hdView.BranchName = Employee.Branch.BranchName;
                hdView.BranchAddress = Employee.Branch.BranchAddress;
                hdView.StoreID = Employee.Branch.StoreID;
                hdView.StoreName = Employee.Branch.Store.StoreName;
                hdView.StoreAddress = Employee.Branch.Store.StoreAddress;
                var tmp = db.Customers.Where(s => s.CustomerID == hdView.CustomerID).Select(s => new
                {
                    FullName = s.LastName+" "+s.MiddleName+" " + s.FirstName
                });
                if (tmp.Count() > 0)
                {
                    hdView.CustomerName = tmp.Select(s => s.FullName).SingleOrDefault();
                }
                db.OrderViewModels.Add(hdView);
                db.SaveChanges();
                //detail view
                
                foreach (var item in lstSession.Items)
                {
                    //order detail view model
                   
                    cthdView = new OrderDetailViewModel();
                    cthdView.OrderDetailID = cthd.OrderDetailID;
                  
                    cthdView.OrderID = hdView.OrderID;
                    cthdView.ProductID = item.Product.ProductID;
                    cthdView.Quantity = item.Quantity;
                    cthdView.Price = item.Value;
                    cthdView.CategoryID = item.Product.CategoryID;
                    cthdView.ProductName = item.Product.ProductName;
                    cthdView.ProductPrice = item.Product.Price;
                    cthdView.ProductTax = item.Product.Tax;
                    cthdView.ProductDiscount = item.Product.Discount;
                    cthdView.CategoryName = item.Product.Category.CategoryName;

                    db.OrderDetailViewModel.Add(cthdView);
                    db.SaveChanges();

                }
                //order view model
                hdView.Quantity = hd.Quantity;
                hdView.Discount = hd.Discount;
                hdView.Total = hd.Total;
                db.SaveChanges();
                //var hds = db.Orders.where
                ViewBag.tong = lstSession.TotalValue;
                ViewBag.TongSL = lstSession.Items.Select(s => s.Quantity).Sum();
                ViewBag.TongTienKM = lstSession.Items.Select(s => s.Discount).Sum();
                ViewBag.TongTienKhachTra = lstSession.TotalValue2();
                var CustomerName = db.Customers.Where(s => s.CustomerID == lstSession.CustomerID).Select(s => new { CustomerName = s.LastName + " " + s.MiddleName + " " + s.FirstName }).FirstOrDefault();
                if(CustomerName!=null)
                {
                ViewBag.CustomerName = CustomerName.CustomerName;
                }
                else
                {
                    ViewBag.CustomerName = "Khách Lẻ";
                }
                Session["shoppingcard"] = null;
                return View(hd);
            }
            else
            {
                return View("Create");
            }
        }
        [HttpPost]
        [Authorize(Roles = "Accountant,Admin")]
        //[ValidateAntiForgeryToken]
        public ActionResult Preview(double Prepay = 0)
        {
            var ID = (int)Session["OrderID"];
           // var CustomerID = (int)Session["CustomerID"];
            Order hd = db.Orders.Where(o => o.OrderID == ID).Select(o => o).FirstOrDefault();
            hd.PrepaymentAmount = Prepay;
           // hd.CustomerID = CustomerID;
            db.SaveChanges();
            Session["OrderID"] = null;
            var infor = new { infor = "ok" };
            return Json(infor, JsonRequestBehavior.AllowGet);
        }
        // Xoa Hoa Don
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order hoaDon = db.Orders.Find(id);

            if (hoaDon == null)
            {
                return HttpNotFound();
            }
            return View(hoaDon);
        }

        // POST: Order/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order hoaDon = db.Orders.Find(id);
            db.Orders.Remove(hoaDon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Order/Details/5
        [Authorize(Roles = "Accountant, Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrderViewModel hds = db.OrderViewModels.Find(id);
            if (hds == null)
            {
                return HttpNotFound();
            }


            return View(hds);
        }

        //autocomplete
        [Authorize(Roles = "Accountant,Admin")]
        public JsonResult GetMaSP()
        {
            var datasourc = db.Products.Select(c => new { ProductID = c.ProductID, ProductName = c.ProductName });
            return Json(datasourc.ToList(), JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Accountant,Admin")]
        public JsonResult GetCustomer()
        {
            var datasourc = db.Customers.Select(c => new { CustomerID = c.CustomerID.ToString(), CustomerName =c.LastName+" "+c.MiddleName+" "+ c.FirstName ,PhoneNumber=c.PhoneNumber});
            return Json(datasourc.ToList(), JsonRequestBehavior.AllowGet);
        }
        
        // delete hoa don
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].OrderID;
            
            
                var order = db.Orders.Find(Id);
                if (order != null)
                {
                    var orderDetail = order.OrderDetails.Select(s => s).ToList();
                    foreach (var item in orderDetail)
                    {
                        db.OrderDetails.Remove(item);
                    }
                    db.Orders.Remove(order);
                }
            var orderView = db.OrderViewModels.Find(Id);
            var orderViewDetail = orderView.OrderDetailViewModels.Select(s => s).ToList();
            foreach (var viewitem in orderViewDetail)
            {
                db.OrderDetailViewModel.Remove(viewitem);
            }

            db.OrderViewModels.Remove(orderView);
            db.SaveChanges();
            return RedirectToAction("dashboard", "home");
        }
       

    }
}
