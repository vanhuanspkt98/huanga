﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaleManagement.Controllers
{
    public class AngularBillController : Controller
    {
        // GET: AngularBill
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetCatalogs()
        {
            var ob=new object[]
            { 
                new  {id=1, type="Work", name="Writing code" },
                new  { id= 2, type= "Work", name= "Testing code" },
                new  { id= 3, type= "Work", name= "Fixing bugs" },
                new  { id= 4, type= "Play", name= "Dancing" }
            };
            return Json(ob,JsonRequestBehavior.AllowGet);
        }
    }
}