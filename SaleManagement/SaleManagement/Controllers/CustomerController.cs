﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class CustomerController : Controller
    {
        private QLBHContext db = new QLBHContext();
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        public JsonResult GetGrid()
        {

            var lst = db.Customers.Include(p=>p.CustomerCategory)
             .Select(s => new
             {
                 CustomerID = s.CustomerID,
                 Email = s.Email,
                 LastName = s.LastName,
                 MiddleName=s.MiddleName,
                 FirstName=s.FirstName,
                 Birthday=s.Birthday,
                 Sex=s.Sex,
                 Address=s.Address,
                 PhoneNumber=s.PhoneNumber,
                 Point=s.Point,
                 CustomerCategoryID = s.CustomerCategoryID
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public JsonResult GetgridCusCate()
        {
            var lst = db.CustomerCategorys.Select(s => new
            {
                CustomerCategoryID = s.CustomerCategoryID,
                CustomerCategoryName = s.CustomerCategoryName
            });
            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create(String models)
        {
           Customer temp = new Customer();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
         //   temp.CustomerID = db.Customers.Max(s=>s.CustomerID)+1;
            temp.Email = lst[0].Email;
            temp.LastName = lst[0].LastName;
            temp.MiddleName = lst[0].MiddleName;
            temp.FirstName = lst[0].FirstName;
            temp.Birthday = lst[0].Birthday;
            temp.Sex = lst[0].Sex;
            temp.Address = lst[0].Address;
            temp.PhoneNumber = lst[0].PhoneNumber;
            temp.CustomerCategoryID = lst[0].CustomerCategoryID;
            temp.Point = 0;
            db.Customers.Add(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].CustomerID;
            var temp = db.Customers.Find(Id);
            temp.Email = lst[0].Email;
            temp.LastName = lst[0].LastName;
            temp.MiddleName = lst[0].MiddleName;
            temp.FirstName = lst[0].FirstName;
            temp.Birthday = lst[0].Birthday;
            temp.Sex = lst[0].Sex;
            temp.Address = lst[0].Address;
            temp.PhoneNumber = lst[0].PhoneNumber;
            temp.CustomerCategoryID = lst[0].CustomerCategoryID;
            //temp.Point = lst[0].Point;
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].CustomerID;
            var sp = db.Customers.Find(Id);
            db.Customers.Remove(sp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        [HttpPost]
        [Authorize(Roles="Admin")]
        public ActionResult EditPoint(int PointValue, int PointRatio)
        {
            var point = db.Points.First();
            point.PointRatio = PointRatio;
           point.PointValue = PointValue;
            db.SaveChanges();
            return RedirectToAction("Index","Home");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult EditPoint()
        {
           var point= db.Points.First();
           return View(point);
        }
        //tính điểm tích lũy
          [Authorize(Roles = "Admin")]
      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
