﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class BranchController : Controller
    {
        private QLBHContext db = new QLBHContext();

        // GET: Branche
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
           
            return View();
        }
        //GET: Store
        [Authorize(Roles = "Admin")]
        public JsonResult GetgridStore()
        {
            var lst = db.Stores
                .Select(s => new
                {
                    StoreID = s.StoreID,
                    StoreName = s.StoreName
                }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public JsonResult GetGrid()
        {

            var lst = db.Branchs.Include(p => p.Store)
             .Select(s => new
             {
                 BranchID = s.BranchID,
                 StoreID = s.StoreID,
                 BranchName = s.BranchName,
                 BranchAddress = s.BranchAddress,
                 PhoneNumber = s.PhoneNumber,
                 StoreName = s.Store.StoreName
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create(String models)
        {
            Branch temp = new Branch();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
           // 
            //var tmp = db.Branchs.Where(s => s.BranchID == Id);
            //them chi nhanh moi
            
            //    temp.BranchID = db.Branchs.Max(s => s.BranchID) + 1;
                temp.StoreID = lst[0].StoreID;
                temp.BranchName = lst[0].BranchName;
                temp.BranchAddress = lst[0].BranchAddress;
                temp.PhoneNumber = lst[0].PhoneNumber;
                db.Branchs.Add(temp);
                db.SaveChanges();
           
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);

            int Id = lst[0].BranchID;
                var temp = db.Branchs.Find(Id);
                temp.BranchID = lst[0].BranchID;
                temp.StoreID = lst[0].StoreID;
                temp.BranchName = lst[0].BranchName;
                temp.BranchAddress = lst[0].BranchAddress;
                temp.PhoneNumber = lst[0].PhoneNumber;
                db.SaveChanges();
            
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].BranchID;
            var temp = db.Branchs.Find(Id);
            var emp = temp.ApplicationUsers.Select(s => s).ToList();
            //delete employee
            foreach (var item in emp)
            {  
                var order = item.Orders.Select(s => s).ToList();
                //delete order
                foreach (var oritem in order)
                {   
                    var orderDetail = oritem.OrderDetails.Select(s => s).ToList();
                    //delete orderdetails
                    foreach (var ordeitem in orderDetail)
                    {
                        db.OrderDetails.Remove(ordeitem);
                    }
                    db.Orders.Remove(oritem);
                }
                
                db.Users.Remove(item);
            }
            db.Branchs.Remove(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
