﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class CategoryController : Controller
    {
        private QLBHContext db = new QLBHContext();
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Index()
        {

            return View();
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public JsonResult GetGrid()
        {

            var lst = db.Categorys
             .Select(s => new
             {
                 CategoryID =s.CategoryID,
                 CategoryName = s.CategoryName,
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Create(String models)
        {
            Category temp = new Category();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            string Id = lst[0].CategoryID; 
            var tmp = db.Categorys.Where(s => s.CategoryID == Id);
            if (tmp.Count() == 0)
            {
                temp.CategoryID = lst[0].CategoryID;
                temp.CategoryName = lst[0].CategoryName;
                db.Categorys.Add(temp);
                db.SaveChanges();
            }
            else
            {
                var ct = db.Categorys.Find(Id);
                ct.CategoryName = lst[0].CategoryName;
                db.SaveChanges();

            }
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);
            String Id = lst[0].CategoryID;
            var tmp = db.Categorys.Where(s => s.CategoryID == Id);
            if (tmp.Count() > 0)
            {
                var temp = db.Categorys.Find(Id);
                temp.CategoryName = lst[0].CategoryName;
                db.SaveChanges();
            }
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            string Id = lst[0].CategoryID;
            var temp = db.Categorys.Find(Id);
            var product = temp.Products.Select(s => s).ToList();
            foreach (var sp in product)
            {
                var prodetails = sp.ProviderDetails.Select(s => s).ToList();
                foreach (var prdeitem in prodetails)
                {
                    db.ProviderDetails.Remove(prdeitem);
                }
                //remove order details
                var order = sp.OrderDetails.Select(s => s).ToList();
                foreach (var oritem in order)
                {
                    db.OrderDetails.Remove(oritem);
                }
                db.Products.Remove(sp);
            }
            db.Categorys.Remove(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
