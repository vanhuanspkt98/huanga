﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class CustomerCategoryController : Controller
    {
        private QLBHContext db = new QLBHContext();

        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public JsonResult GetGrid()
        {

            var lst = db.CustomerCategorys
             .Select(s => new
             {
                 CustomerCategoryID = s.CustomerCategoryID,
                 CustomerCategoryName = s.CustomerCategoryName,
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Create(String models)
        {
            CustomerCategory temp = new CustomerCategory();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            String Id = lst[0].CustomerCategoryID;
            var tmp = db.CustomerCategorys.Where(s=>s.CustomerCategoryID==Id);
            if (tmp.Count() == 0)
            {
                temp.CustomerCategoryID = lst[0].CustomerCategoryID;
                temp.CustomerCategoryName = lst[0].CustomerCategoryName;
                db.CustomerCategorys.Add(temp);
                db.SaveChanges();
            }
            else
            {
                var ct = db.CustomerCategorys.Find(Id);
                ct.CustomerCategoryName = lst[0].CustomerCategoryName;
                db.SaveChanges();
            }
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);
            string Id = lst[0].CustomerCategoryID;
            var temp = db.CustomerCategorys.Find(Id);
            temp.CustomerCategoryName = lst[0].CustomerCategoryName;
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            String Id = lst[0].CustomerCategoryID;
            var tmp = db.CustomerCategorys.Find(Id);
            var cus = tmp.Customers.Select(s => s).ToList();
            foreach (var cusitem in cus)
            {
                db.Customers.Remove(cusitem);
            }
            db.CustomerCategorys.Remove(tmp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
