﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class StoreController : Controller
    {
        private QLBHContext db = new QLBHContext();

        // GET: Store
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {

            return View();
        }
        [Authorize(Roles = "Admin")]
        public JsonResult GetGrid()
        {

            var lst = db.Stores
             .Select(s => new
             {
                 StoreID = s.StoreID,
                 StoreName = s.StoreName,
                 StoreAddress = s.StoreAddress,
                 PhoneNumber = s.PhoneNumber,
                 Website = s.Website
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create(String models)
        {
            Store temp = new Store();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            temp.StoreID = db.Stores.Max(s => s.StoreID) + 1;
            temp.StoreName = lst[0].StoreName;
            temp.StoreAddress = lst[0].StoreAddress;
            temp.PhoneNumber = lst[0].PhoneNumber;
            temp.Website = lst[0].Website;
            db.Stores.Add(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].StoreID;
            var temp = db.Stores.Find(Id);
            temp.StoreName = lst[0].StoreName;
            temp.StoreAddress = lst[0].StoreAddress;
            temp.PhoneNumber = lst[0].PhoneNumber;
            temp.Website = lst[0].Website;
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].StoreID;
            var temp = db.Stores.Find(Id);
            var br = temp.Branchs.Select(s => s).ToList();
            foreach (var britem in br)
            {
                var emp = britem.ApplicationUsers.Select(s => s).ToList();
                //delete employee
                foreach (var item in emp)
                {
                    var order = item.Orders.Select(s => s).ToList();
                    //delete order
                    foreach (var oritem in order)
                    {
                        var orderDetail = oritem.OrderDetails.Select(s => s).ToList();
                        //delete orderdetails
                        foreach (var ordeitem in orderDetail)
                        {
                            db.OrderDetails.Remove(ordeitem);
                        }
                        db.Orders.Remove(oritem);
                    }

                    db.Users.Remove(item);
                }
                db.Branchs.Remove(britem);
            }
            db.Stores.Remove(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
