﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class ProviderDetailController : Controller
    {
        private QLBHContext db = new QLBHContext();

        // GET: ProviderDetailDetail
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Index()
        {

            return View();
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public JsonResult GetGrid()
        {

            var lst = db.ProviderDetails.Include(s=>s.Product).Include(s=>s.Provider)
             .Select(s => new
             {
                 ProviderDetailID = s.ProviderDetailID,
                 ProviderID = s.ProviderID,
                 ProductID = s.ProductID,
                 Quantity = s.Quantity,
                 Price = s.Price
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Create(String models)
        {
            ProviderDetail temp = new ProviderDetail();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            temp.ProviderDetailID = db.ProviderDetails.Max(s => s.ProviderDetailID) + 1;
            temp.ProviderID = lst[0].ProviderID;
            temp.ProductID = lst[0].ProductID;
            temp.Quantity = lst[0].Quantity;
            temp.Price = lst[0].Price;
            db.ProviderDetails.Add(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].ProviderDetailID;
            var temp = db.ProviderDetails.Find(Id);
            temp.ProviderID = lst[0].ProviderID;
            temp.ProductID = lst[0].ProductID;
            temp.Quantity = lst[0].Quantity;
            temp.Price = lst[0].Price;
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].ProviderDetailID;
            var temp = db.ProviderDetails.Find(Id);
            db.ProviderDetails.Remove(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
