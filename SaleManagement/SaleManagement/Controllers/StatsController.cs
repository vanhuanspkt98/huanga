﻿using Newtonsoft.Json;
using SaleManagement.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SaleManagement.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StatsController : Controller
    {

        private QLBHContext db = new QLBHContext();

        #region ACTION RESULT
        // GET: Stats
        public ActionResult Index()
        {
            DateTime thisweek = DateTime.Now.AddDays(-6);
            var lst = db.OrderDetailViewModel.Where(s => DateTime.Compare(s.OrderViewModel.DateOfOrder, thisweek) >= 0
                                            && DateTime.Compare(s.OrderViewModel.DateOfOrder, DateTime.Now) <= 0)
                                            .GroupBy(s => s.ProductID).Select(s => new
                                            {
                                                ProductID = s.Key,
                                                ProductName = s.Select(a => a.ProductName).FirstOrDefault(),
                                                Quantity = s.Sum(a => (int?)a.Quantity ?? 0)
                                            }).OrderByDescending(s => s.Quantity).ToList();
            if (lst.Count() > 0)
            {
                ViewBag.Product1 = lst[0].ProductName;
                ViewBag.Quantity1 = lst[0].Quantity;
            }
            if (lst.Count() > 1)
            {
                ViewBag.Product2 = lst[1].ProductName;
                ViewBag.Quantity2 = lst[1].Quantity;
            }
            if (lst.Count() > 2)
            {
                ViewBag.Product3 = lst[2].ProductName;
                ViewBag.Quantity3 = lst[2].Quantity;
            }
            if (lst.Count() > 3)
            {
                ViewBag.Product4 = lst[3].ProductName;
                ViewBag.Quantity4 = lst[3].Quantity;
            }
            return View();
        }
        //1.0 list for year 
        public ActionResult List()
        {
            return View();
        }
        //1.1 list for month
        public ActionResult ListMonth()
        {
            return View();
        }
        //1.2 list for day
        public ActionResult ListDay()
        {
            return View();
        }
        //1.3 list day by day
        public ActionResult ListDayByDay()
        {
            return View();
        }
        //1.4 list for customer
        public ActionResult ListCustomer()
        {
            return View();
        }
        // GET: OrderViewModel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrderViewModel hds = db.OrderViewModels.Find(id);
            if (hds == null)
            {
                return HttpNotFound();
            }


            return View(hds);
        }
        //2.0 graph for DT
        public ActionResult Graph()
        {

            return View();
        }
        //2.1 graph for So luong san pham
        public ActionResult GraphProductQuantity()
        {
            return View();
        }
        //2.2 Graph SanPham
        public ActionResult GraphProduct()
        {
            return View();
        }
        //2.3 Graph Thanh Khoản
        public ActionResult GraphLiquidity()
        {
            return View();
        }
        #endregion
        #region JSON
        // A. Index Thanh Khoan
        public JsonResult LiquidityOfWeek()
        {

            var thisweek = DateTime.Now.AddDays(-6);
            var lastweek = thisweek.AddDays(-7);
            //thanh khoản tuan nay
            var lst = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, DateTime.Now) <= 0
                                        && DateTime.Compare(s.DateOfOrder, thisweek) >= 0).Select(s => new
                                        {
                                            Date = s.DateOfOrder,
                                            Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month,
                                            Liquidity = s.OrderID
                                        }).GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                                        {
                                            Time = s.Key,
                                            Date = s.Select(a => a.Date).FirstOrDefault(),
                                            Liquidity = s.Select(a => a.Liquidity).Count(),
                                            LiquidityLastWeek = 0
                                        }).ToList();
            //thanh khoan tuan truoc
            var lst1 = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, thisweek) < 0
                                       && DateTime.Compare(s.DateOfOrder, lastweek) >= 0).Select(s => new
                                       {
                                           Date = s.DateOfOrder,
                                           Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month,
                                           Liquidity = s.OrderID
                                       }).GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                                       {
                                           Time = s.Key,
                                           Date = s.Select(a => a.Date).FirstOrDefault(),
                                           Liquidity = s.Select(a => a.Liquidity).Count()
                                       }).ToList();

            List<object> lstObj = new List<object>();
            
            for (DateTime i = thisweek, j = lastweek; DateTime.Compare(i, DateTime.Now) <= 0 && DateTime.Compare(j, thisweek) < 0; i = i.AddDays(1), j = j.AddDays(1))
            {

                var tmp = lst.Where(s => s.Date.Day == i.Day).Select(s => s).FirstOrDefault();
                var tmp2 = lst1.Where(s => s.Date.Day == j.Day).Select(s => s).FirstOrDefault();


                if (tmp != null || tmp2 != null)
                {
                    int x1 = 0, x2 = 0;
                    if (tmp != null)
                        x1 = tmp.Liquidity;
                    if (tmp2 != null)
                        x2 = tmp2.Liquidity;
                    var x = new
                    {
                        Day = i.Day,
                        Time = i.Day + "/" + i.Month,
                        Liquidity = x1,
                        LiquidityLastWeek = x2
                    };
                    lstObj.Add(x);
                }
                else
                {
                    var x = new
                    {
                        Day = i.Day,
                        Time = i.Day + "/" + i.Month,
                        Liquidity = 0,
                        LiquidityLastWeek = 0
                    };
                    lstObj.Add(x);
                }
            }
            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        //A. Index Doanh Thu Theo Tuan
        public JsonResult TurnoverOfWeek()
        {

            var thisweek = DateTime.Now.AddDays(-6);
            var lastweek = thisweek.AddDays(-7);
            //doanh thu tuan nay
            var lst = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, DateTime.Now) <= 0
                                        && DateTime.Compare(s.DateOfOrder, thisweek) >= 0).Select(s => new
                                        {
                                            Date = s.DateOfOrder,
                                            Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month,
                                            Turnover = s.Total
                                        }).GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                                        {
                                            Time = s.Key,
                                            Date = s.Select(a => a.Date).FirstOrDefault(),
                                            Turnover = s.Sum(a => (int?)a.Turnover) ?? 0,
                                            TurnoverLastWeek = 0
                                        }).ToList();
            //doanh thu tuan truoc
            var lst1 = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, thisweek) <= 0
                                       && DateTime.Compare(s.DateOfOrder, lastweek) > 0).Select(s => new
                                       {
                                           Date = s.DateOfOrder,
                                           Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month,
                                           Turnover = s.Total
                                       }).GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                                       {
                                           Time = s.Key,
                                           Date = s.Select(a => a.Date).FirstOrDefault(),
                                           Turnover = s.Sum(a => (int?)a.Turnover) ?? 0
                                       }).ToList();

            List<object> lstObj = new List<object>();
            var r = DateTime.Compare(DateTime.Parse("4/30/2015"), DateTime.Parse("5/1/2015"));
            
            for (DateTime i = thisweek, j = lastweek; DateTime.Compare(i, DateTime.Now) <= 0 && DateTime.Compare(j, thisweek) < 0; i = i.AddDays(1), j = j.AddDays(1))
            {

                var tmp = lst.Where(s => s.Date.Day == i.Day).Select(s => s).FirstOrDefault();
                var tmp2 = lst1.Where(s => s.Date.Day == j.Day).Select(s => s).FirstOrDefault();
                int x1 = 0, x2 = 0;
                
                if (tmp != null || tmp2 != null)
                {
                    if (tmp != null)
                    {
                        x1 = tmp.Turnover;
                    }
                    if (tmp2 != null)
                    {
                        x2 = tmp2.Turnover;
                    }
                    var x = new
                    {
                        Day = i.Day,
                        Time = i.Day + "/" + i.Month,
                        Turnover = x1,
                        TurnoverLastWeek = x2
                    };
                    lstObj.Add(x);
                }
                else
                {
                    var x = new
                    {
                        Day = i.Day,
                        Time = i.Day + "/" + i.Month,
                        Turnover = 0,
                        TurnoverLastWeek = 0
                    };
                    lstObj.Add(x);
                }
            }
            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }

        // Index số lượng sp theo tuan
        public ActionResult CountProductWeek()
        {
            var thisweek = DateTime.Now.AddDays(-6);
            int CountProduct = 0;
            var lst = db.OrderDetailViewModel.Where(s => DateTime.Compare(s.OrderViewModel.DateOfOrder, thisweek) >= 0
                                            && DateTime.Compare(s.OrderViewModel.DateOfOrder, DateTime.Now) <= 0)
                                            .ToList();
            if (lst.Count() > 0)
            {
                CountProduct = lst.Sum(s => (int?)s.Quantity ?? 0);
            }
            String str = CountProduct.ToString("N1", CultureInfo.InvariantCulture);
            return PartialView(str as object);
        }
        // Index so luong sp tuan truoc
        public ActionResult CountProductLastWeek()
        {
            DateTime thisweek = DateTime.Now.AddDays(-6);
            DateTime lastweek = thisweek.AddDays(-7);
            int SLTuanTruoc = 0;
            int SLTuanNay = 0;
            double Sum = 100;
            String str = null;
            //tuan nay
            var _tuanNay = db.OrderDetailViewModel.Where(s => DateTime.Compare(s.OrderViewModel.DateOfOrder, thisweek) >= 0
                                            && DateTime.Compare(s.OrderViewModel.DateOfOrder, DateTime.Now) <= 0).ToList();
            //tuan truoc
            var _tuanTruoc = db.OrderDetailViewModel.Where(s => DateTime.Compare(s.OrderViewModel.DateOfOrder, lastweek) >= 0
                                            && DateTime.Compare(s.OrderViewModel.DateOfOrder, thisweek) <= 0).ToList();
            if (_tuanNay.Count() > 0)
            {
                SLTuanNay = _tuanNay.Sum(s => (int?)s.Quantity ?? 0);
            }
            if (_tuanTruoc.Count() > 0)
            {
                SLTuanTruoc = _tuanTruoc.Sum(s => (int?)s.Quantity ?? 0);
            }
            if (SLTuanTruoc > 0)
            {
                Sum =(float) (SLTuanNay - SLTuanTruoc)/(SLTuanNay+SLTuanTruoc) *100;
            }
            if (SLTuanNay == 0 && SLTuanTruoc == 0)
            {
                Sum = 0;
            }
            Sum = Math.Round(Sum, 2);
            if (Sum >= 0)
            {
                str = "▲" + Sum + "%";
            }
            else
            {
                Sum = -Sum;
                str = "▼" + Sum + "%";
            }
            return PartialView(str as object);

        }
        // san pham ban chay

        //luong giao dich tuan nay
        public ActionResult CountLiquidityWeek()
        {
            DateTime thisweek = DateTime.Now.AddDays(-6);
            int Count = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, thisweek) >= 0
                                         && DateTime.Compare(s.DateOfOrder, DateTime.Now) <= 0).Select(s => s.OrderID).Count();
            String str = Count.ToString("N1", CultureInfo.InvariantCulture);
            return PartialView(str as object);
        }
        //luong giao dich tuan truoc
        public ActionResult CountLiquidityLastWeek()
        {
            DateTime thisweek = DateTime.Now.AddDays(-6);
            DateTime lastweek = thisweek.AddDays(-7);
            //tuan nay
            int Count1 = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, thisweek) >= 0
                                         && DateTime.Compare(s.DateOfOrder, DateTime.Now) <= 0).Select(s => s.OrderID).Count();
            //tuan truoc
            int Count2 = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, lastweek) > 0
                                         && DateTime.Compare(s.DateOfOrder, thisweek) <= 0).Select(s => s.OrderID).Count();
            double Count = 100;
            if (Count2 > 0)
            {
                Count = (float)(Count1 - Count2)/(Count1+Count2)*100;
            }

            Count = Math.Round(Count, 2);
            if (Count1 == 0 && Count2 == 0)
            {
                Count = 0;
            }
            String str = null;
            if (Count > 0)
            {
                str = "▲" + Count + "%";
            }
            else
            {
                Count = -Count;
                str = "▼" + Count + "%";
            }
            return PartialView(str as object);
        }

        //A.0 doanh thu tuan nay
        public ActionResult SumTurnoverWeek()
        {
            DateTime thisweek = DateTime.Now.AddDays(-6);
            double Sum = 0;
            var lst = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, thisweek) >= 0
                                         && DateTime.Compare(s.DateOfOrder, DateTime.Now) <= 0).ToList();
            if (lst.Count() > 0)
            {
                Sum = lst.Sum(s => (int?)s.Total ?? 0);
            }
            string currency = string.Format("{0:#,##0.00₫;(##0.0,#₫);''}", Sum);
            return PartialView(currency as object);
        }
        //A.1 doanh thu tuan truoc
        public ActionResult SumTurnoverLastWeek()
        {
            DateTime thisweek = DateTime.Now.AddDays(-6);
            DateTime lastweek = thisweek.AddDays(-7);
            double Sum1 = 0;
            double Sum2 = 0;
            double Sum = 100;
            String str = null;
            //tuan nay
            var _tuanNay = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, thisweek) >= 0
                                         && DateTime.Compare(s.DateOfOrder, DateTime.Now) <= 0).ToList();
            //tuan truoc
            var _tuanTruoc = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, lastweek) > 0
                                         && DateTime.Compare(s.DateOfOrder, thisweek) <= 0).ToList();

            if (_tuanNay.Count() > 0)
            {
                Sum1 = _tuanNay.Sum(s => (int?)s.Total ?? 0);
            }
            if (_tuanTruoc.Count() > 0)
            {
                Sum2 = _tuanTruoc.Sum(s => (int?)s.Total ?? 0);
            }
            if (Sum2 > 0)
            {
                Sum =(float) (Sum1 - Sum2)/(Sum1+Sum2)*100;
            }
            Sum = Math.Round(Sum, 2);
            if (Sum1 == 0 && Sum2 == 0)
            {
                Sum = 0;
            }
            if (Sum > 0)
            {
                str = "▲" + Sum + "%";
            }
            else
            {
                Sum = -Sum;
                str = "▼" + Sum + "%";
            }
            return PartialView(str as object);
        }

        //0.0 Graph Doanh Thu by year
        public JsonResult GraphDTByYear(int year)
        {
            var lstOder = db.OrderViewModels.Where(s => s.DateOfOrder.Year == year).Select(s => new
            {

                Month = s.DateOfOrder.Month,
                Time = s.DateOfOrder.Month + "/" + s.DateOfOrder.Year,
                SumOfProductQuantity = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
                Turnover = s.Total

            })
                 .GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                 {
                     Time = s.Key,
                     Month = s.Select(a => a.Month).FirstOrDefault(),
                     Turnover = s.Select(a => a.Turnover).Sum(),
                     SOPQ = s.Sum(a => (int?)a.SumOfProductQuantity) ?? 0
                 }).ToList();
            List<object> lstObj = new List<object>();
            for (int i = 0; i < 12; i++)
            {
                var tmp = lstOder.Where(s => s.Month == i + 1).Select(s => s).FirstOrDefault();
                if (tmp != null)
                {
                    lstObj.Add(tmp);
                }
                else
                {
                    var x = new
                    {
                        Time = i + 1,
                        Turnover = 0,
                        Month = i + 1,
                        SOPQ = 0
                    };
                    lstObj.Add(x);
                }

            }
            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        //0.1 Graph Doanh Thu by Month
        public JsonResult GraphDTbyMonth(DateTime month)
        {
            var lstOrder = db.OrderViewModels.Where(s => s.DateOfOrder.Year == month.Year
                                       && s.DateOfOrder.Month == month.Month).Select(s => new
           {

               Day = s.DateOfOrder.Day,
               SumOfProductQuantity = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
               Turnover = s.Total

           })
                .GroupBy(s => s.Day).OrderBy(a => a.Key).Select(s => new
                {
                    Day = s.Key,
                    Turnover = s.Select(a => a.Turnover).Sum(),
                    SOPQ = s.Sum(a => (int?)a.SumOfProductQuantity) ?? 0
                }).ToList();
            List<object> lstObj = new List<object>();
            for (int i = 0; i < 31; i++)
            {
                var x = new
                {
                    Turnover = 0,
                    Day = i + 1,
                    SOPQ = 0
                };
                lstObj.Add(x);
            }
            for (int i = 0; i < lstOrder.Count() && i < 31; i++)
            {
                var tmp = lstOrder[i].Day;

                lstObj[tmp - 1] = lstOrder[i];

            }
            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        //0.2 Graph Doanh Thu by Day
        public JsonResult GraphDTbyDay(DateTime day)
        {
            var lstOrder = db.OrderViewModels.Where(s => s.DateOfOrder.Year == day.Year
                                              && s.DateOfOrder.Month == day.Month
                                              && s.DateOfOrder.Day == day.Day).Select(s => new
            {

                Hour = s.DateOfOrder.Hour,
                SumOfProductQuantity = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
                Turnover = s.Total

            })
                 .GroupBy(s => s.Hour).OrderBy(a => a.Key).Select(s => new
                 {
                     Hour = s.Key,
                     Turnover = s.Select(a => a.Turnover).Sum(),
                     SOPQ = s.Sum(a => (int?)a.SumOfProductQuantity) ?? 0
                 }).ToList();
            List<object> lstObj = new List<object>();
            for (int i = 0; i < 24; i++)
            {
                var x = new
                {
                    Hour = i,
                    Turnover = 0,
                    SOPQ = 0
                };
                lstObj.Add(x);
            }
            for (int i = 0; i < lstOrder.Count(); i++)
            {
                var tmp = lstOrder[i].Hour;

                lstObj[tmp] = lstOrder[i];

            }
            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        //0.3 Graph doanh thu from date to date
        public JsonResult GraphDTDaybyDay(DateTime from, DateTime to)
        {
            var lst = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, from) >= 0
                                        && DateTime.Compare(s.DateOfOrder, to) <= 0).Select(s => new
                                        {
                                            Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month + "/" + s.DateOfOrder.Year,
                                            Date = s.DateOfOrder,
                                            Day = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month,
                                            Month = s.DateOfOrder.Month + "/" + s.DateOfOrder.Year,
                                            Turnover = s.Total
                                        }).GroupBy(s => s.Time).OrderBy(s => s.Key).Select(s => new
                                        {
                                            Time = s.Key,
                                            Date = s.Select(a => a.Date).FirstOrDefault(),
                                            Turnover = s.Sum(a => (int?)a.Turnover ?? 0)
                                        })
                                        .ToList();
            List<object> lstObj = new List<object>();
            for (DateTime i = from; DateTime.Compare(i, to) <= 0; i = i.AddDays(1))
            {
                var tmp = lst.Where(s => s.Date.Day == i.Day && s.Date.Month == i.Month && s.Date.Year == i.Year).Select(s => s).FirstOrDefault();
                if (tmp != null)
                {
                    var x = new
                    {

                        Time = tmp.Time,
                        Date = tmp.Date,
                        Turnover = tmp.Turnover
                    };
                    lstObj.Add(x);
                }
                else
                {
                    var x = new
                    {
                        Time = i.Day + "/" + i.Month + "/" + i.Year,
                        Date = i,
                        Turnover = 0
                    };
                    lstObj.Add(x);
                }
            }
            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }

        //1.0 Graph Sản Lượng Từng Sản Phẩm Bán Ra
        // trong năm
        public JsonResult GraphProductByYear(int year, string category)
        {
            if (category == null)
            {
                category = db.Products.Select(s => s.CategoryID).FirstOrDefault();
            }
            var lst = db.OrderDetailViewModel.Where(s => s.OrderViewModel.DateOfOrder.Year == year
                                               && s.CategoryID == category).Select(s => new
            {
                ProductID = s.ProductID,
                Quantity = s.Quantity,
                Date = s.OrderViewModel.DateOfOrder,
                ProductName = s.ProductName
            })
            .GroupBy(s => s.ProductID).OrderBy(a => a.Key).Select(s => new
            {
                ProductID = s.Key,
                Date = s.Select(a => a.Date).FirstOrDefault(),
                ProductName = s.Select(a => a.ProductName).FirstOrDefault(),
                Quantity = s.Sum(a => (int?)a.Quantity) ?? 0
            });
            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
        }
        //Trong Thang
        public JsonResult GraphProductByMonth(DateTime month, string category)
        {
            if (category == null)
            {
                category = db.Products.Select(s => s.CategoryID).FirstOrDefault();
            }
            var lstOrder = db.OrderDetailViewModel.Where(s => s.OrderViewModel.DateOfOrder.Year == month.Year
                                       && s.OrderViewModel.DateOfOrder.Month == month.Month
                                       && s.CategoryID == category).Select(s => new
                                       {
                                           ProductID = s.ProductID,
                                           Quantity = s.Quantity,
                                           // Date = s.DateOfOrder ,
                                           ProductName = s.ProductName
                                       })
                .GroupBy(s => s.ProductID).OrderBy(a => a.Key).Select(s => new
                {
                    ProductID = s.Key,
                    ProductName = s.Select(a => a.ProductName).FirstOrDefault(),
                    Quantity = s.Sum(a => (int?)a.Quantity) ?? 0
                });
            return Json(lstOrder.ToList(), JsonRequestBehavior.AllowGet);
        }
        //trong ngày
        public JsonResult GraphProductByDay(DateTime day, string category)
        {
            if (category == null)
            {
                category = db.Products.Select(s => s.CategoryID).FirstOrDefault();
            }
            var lstOrder = db.OrderDetailViewModel.Where(s => s.OrderViewModel.DateOfOrder.Year == day.Year
                                       && s.OrderViewModel.DateOfOrder.Month == day.Month
                                       && s.OrderViewModel.DateOfOrder.Day == day.Day
                                       && s.CategoryID == category).Select(s => new
                                       {
                                           ProductID = s.ProductID,
                                           Quantity = s.Quantity,
                                           // Date = s.DateOfOrder ,
                                           ProductName = s.ProductName
                                       })
                .GroupBy(s => s.ProductID).OrderBy(a => a.Key).Select(s => new
                {
                    ProductID = s.Key,
                    ProductName = s.Select(a => a.ProductName).FirstOrDefault(),
                    Quantity = s.Sum(a => (int?)a.Quantity) ?? 0
                });
            return Json(lstOrder.ToList(), JsonRequestBehavior.AllowGet);
        }
        //1.1 Graph thanh khoản
        //trong năm
        public JsonResult GraphLiquidityByYear(int year)
        {
            var lst = db.OrderViewModels.Where(s => s.DateOfOrder.Year == year).Select(s => new
            {
                CountOrder = s.OrderID,
                Time = s.DateOfOrder.Month
            })
                 .GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                 {
                     Time = s.Key,
                     CountOrder = s.Select(a => a.CountOrder).Count()
                 }).ToList();
            List<object> lstObj = new List<object>();
            for (int i = 0; i < 12; i++)
            {
                var x = new
                {

                    Time = i + 1,
                    CountOrder = 0
                };
                lstObj.Add(x);
            }
            for (int i = 0; i < lst.Count(); i++)
            {
                var tmp = lst[i].Time;

                lstObj[tmp - 1] = lst[i];

            }

            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        //trong tháng
        public JsonResult GraphLiquidityByMonth(DateTime month)
        {
            var lst = db.OrderViewModels.Where(s => s.DateOfOrder.Year == month.Year
                                        && s.DateOfOrder.Month == month.Month).Select(s => new
            {
                CountOrder = s.OrderID,
                Time = s.DateOfOrder.Month
            })
                .GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                {
                    Time = s.Key,
                    CountOrder = s.Select(a => a.CountOrder).Count()
                }).ToList();
            List<object> lstObj = new List<object>();
            for (int i = 0; i < 31; i++)
            {
                var x = new
                {

                    Time = i + 1,
                    CountOrder = 0
                };
                lstObj.Add(x);
            }
            for (int i = 0; i < lst.Count(); i++)
            {
                var tmp = lst[i].Time;

                lstObj[tmp - 1] = lst[i];

            }

            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        //trong ngày
        public JsonResult GraphLiquidityByDay(DateTime day)
        {
            var lst = db.OrderViewModels.Where(s => s.DateOfOrder.Year == day.Year
                                        && s.DateOfOrder.Month == day.Month
                                        && s.DateOfOrder.Day == day.Day).Select(s => new
            {
                CountOrder = s.OrderID,
                Time = s.DateOfOrder.Hour
            })
                .GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                {
                    Time = s.Key,
                    CountOrder = s.Select(a => a.CountOrder).Count()
                }).ToList();
            List<object> lstObj = new List<object>();
            for (int i = 0; i < 24; i++)
            {
                var x = new
                {

                    Time = i,
                    CountOrder = 0
                };
                lstObj.Add(x);
            }
            for (int i = 0; i < lst.Count(); i++)
            {
                var tmp = lst[i].Time;

                lstObj[tmp] = lst[i];

            }

            return Json(lstObj.ToList(), JsonRequestBehavior.AllowGet);
        }
        // LIST ÉO PHẢI GRAPH
        //.2 List by year - month
        public JsonResult GetDataByYear(int year)
        {
            // List<object> lstObj = new List<object>();
            var lstOder = db.OrderViewModels.Where(s => s.DateOfOrder.Year == year).Select(s => new
            {
                Month = s.DateOfOrder.Month,
                Date = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month + "/" + s.DateOfOrder.Year,
                SumOfProductQuantity = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
                Total = s.Total
            })
                .GroupBy(s => s.Date).OrderBy(a => a.Key).Select(s => new
                {
                    Date = s.Key,
                    Total = s.Select(a => a.Total).Sum(),
                    Month = s.Select(a => a.Month).Distinct(),
                    SOPQ = s.Sum(a => (int?)a.SumOfProductQuantity) ?? 0
                }).ToList();

            return Json(lstOder.ToList(), JsonRequestBehavior.AllowGet);
        }
        //.3 List by month
        public JsonResult GetDataByMonth(DateTime month)
        {
            var lstOrder = db.OrderViewModels.Where(s => s.DateOfOrder.Month == month.Month
                                         && s.DateOfOrder.Year == month.Year).Select(s => new
                                         {
                                             Day = s.DateOfOrder.Day,
                                             Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month + "/" + s.DateOfOrder.Year,
                                             SumOfProductQuantity = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
                                             Total = s.Total
                                         }).GroupBy(s => s.Time).OrderBy(a => a.Key).Select(s => new
                                         {
                                             Time = s.Key,
                                             Total = s.Select(a => a.Total).Sum(),
                                             Day = s.Select(a => a.Day).FirstOrDefault(),
                                             SOPQ = s.Sum(a => (int?)a.SumOfProductQuantity) ?? 0
                                         }).ToList();

            return Json(lstOrder.ToList(), JsonRequestBehavior.AllowGet);
        }
        //.4 List by day
        public JsonResult GetDataByDay(DateTime day)
        {
            var lstOrder = db.OrderViewModels.Where(s => s.DateOfOrder.Year == day.Year
                && s.DateOfOrder.Month == day.Month
                && s.DateOfOrder.Day == day.Day).Select(s => new
                                         {
                                             OrderID = s.OrderID,
                                             EmployeeName = s.EmployeeName,
                                             Date = s.DateOfOrder,
                                             SOPQ = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
                                             Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month + "/" + s.DateOfOrder.Year + " " + s.DateOfOrder.Hour + ":" + s.DateOfOrder.Minute + ":" + s.DateOfOrder.Second,
                                             Total = s.Total
                                         })

                                         .ToList();

            return Json(lstOrder.ToList(), JsonRequestBehavior.AllowGet);
        }
        //.4 List day by day
        public JsonResult GetDataDaybyDay(DateTime from, DateTime to)
        {
            to = to.AddDays(1);
            var lstOrder = db.OrderViewModels.Where(s => DateTime.Compare(s.DateOfOrder, from) >= 0
                && DateTime.Compare(s.DateOfOrder, to) <= 0).Select(s => new
                {
                    OrderID = s.OrderID,
                    EmployeeName = s.EmployeeName,
                    Date = s.DateOfOrder,
                    SumOfProductQuantity = s.OrderDetailViewModels.Sum(a => (int?)a.Quantity) ?? 0,
                    Time = s.DateOfOrder.Day + "/" + s.DateOfOrder.Month + "/" + s.DateOfOrder.Year + " " + s.DateOfOrder.Hour + ":" + s.DateOfOrder.Minute + ":" + s.DateOfOrder.Second,
                    Total = s.Total
                })

                                         .ToList();

            return Json(lstOrder.ToList(), JsonRequestBehavior.AllowGet);
        }
        //.5 List Customer
        public JsonResult GetCustomer1()
        {
            var lst = db.OrderViewModels.Join(db.Customers,
                a => a.CustomerID,
                b => b.CustomerID, (a, b) => new
                {
                    OrderID = a.OrderID,
                    Total = a.Total,
                    Quantity = a.Quantity,
                    Discount = a.Discount,
                    EmployeeName = a.EmployeeName,
                    DepartmentName = a.DepartmentName,
                    CustomerName = b.LastName+" "+b.MiddleName+" "+b.FirstName,
                    CustomerID = a.CustomerID,
                    Email = b.Email,
                    Birthday = b.Birthday,
                    Sex = b.Sex,
                    Address = b.Address,
                    PhoneNumber =b.PhoneNumber,
                    CustomerCategoryName =b.CustomerCategory.CustomerCategoryName,
                    Point=b.Point

                }).OrderByDescending(a=>a.CustomerID).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        //.5 List Customer
        public JsonResult GetCustomer()
        {
            var lst = db.OrderViewModels.Join(db.Customers,
                a => a.CustomerID,
                b => b.CustomerID, (a, b) => new
                {
                    OrderID = a.OrderID,
                    Total = a.Total,
                    Quantity = a.Quantity,
                    EmployeeName = a.EmployeeName,
                    CustomerName = b.LastName + " " + b.MiddleName + " " + b.FirstName,
                    CustomerID = a.CustomerID,
                    Email = b.Email,
                    Birthday = b.Birthday,
                    Sex = b.Sex,
                    Address = b.Address,
                    PhoneNumber = b.PhoneNumber,
                    CustomerCategoryName = b.CustomerCategory.CustomerCategoryName,
                    Point = b.Point

                }).GroupBy(s=>s.CustomerID)
                .OrderBy(s=>s.Key).Select(s=>new{
                    OrderID = s.Select(a=>a.OrderID).Count(),
                    Total = s.Sum(a=>(int?)a.Total??0),
                    Quantity = s.Sum(a=>(int?)a.Quantity??0),
                    EmployeeName = s.Select(a=>a.EmployeeName).FirstOrDefault(),
                    CustomerName = s.Select(a=>a.CustomerName).FirstOrDefault(),
                    CustomerID = s.Select(a=>a.CustomerID).FirstOrDefault(),
                    Email = s.Select(a=>a.Email).FirstOrDefault(),
                    Birthday =s.Select(a=>a.Birthday).FirstOrDefault(),
                    Sex = s.Select(a=>a.Sex).FirstOrDefault(),
                    Address =s.Select(a=>a.Address).FirstOrDefault(),
                    PhoneNumber = s.Select(a=>a.PhoneNumber).FirstOrDefault(),
                    CustomerCategoryName = s.Select(a=>a.CustomerCategoryName).FirstOrDefault(),
                    Point = s.Select(a => a.Point).FirstOrDefault(),
                }).OrderByDescending(s=>s.Total).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion
         //test
        public ActionResult Test()
        {
            // return Json(DateTime.Now, JsonRequestBehavior.AllowGet);
            return View();
        }

        public JsonResult TestGraph()
        {
            var lstOrder = db.OrderViewModels.Select(s => new
                                      {

                                          date = s.DateOfOrder,
                                          Turnover = s.Total
                                      })
               .GroupBy(s => s.date).OrderBy(a => a.Key).Select(s => new
               {
                   date = s.Key,
                   Turnover = s.Select(a => a.Turnover).Sum()
               }).ToList();

            return Json(lstOrder.ToList(), JsonRequestBehavior.AllowGet);
        }


    }
}