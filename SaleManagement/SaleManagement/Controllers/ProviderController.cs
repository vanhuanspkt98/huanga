﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class ProviderController : Controller
    {
        private QLBHContext db = new QLBHContext();

        // GET: Provider
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Index()
        {

            return View();
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public JsonResult GetGrid()
        {

            var lst = db.Providers
             .Select(s => new
             {
                 ProviderID = s.ProviderID,
                 ProviderName = s.ProviderName,
                 ProviderAddress = s.ProviderAddress,
                 PhoneNumber = s.PhoneNumber,
                 Email = s.Email
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Create(String models)
        {
            Provider temp = new Provider();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            temp.ProviderName = lst[0].ProviderName;
            temp.ProviderAddress = lst[0].ProviderAddress;
            temp.PhoneNumber = lst[0].PhoneNumber;
            temp.Email = lst[0].Email;
            db.Providers.Add(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Update(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].ProviderID;
            var temp = db.Providers.Find(Id);
            temp.ProviderName = lst[0].ProviderName;
            temp.ProviderAddress = lst[0].ProviderAddress;
            temp.PhoneNumber = lst[0].PhoneNumber;
            temp.Email = lst[0].Email;
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].ProviderID;
            var temp = db.Providers.Find(Id);
            var prodetails = temp.ProviderDetails.Select(s => s).ToList();
            foreach (var prdeitem in prodetails)
            {
                db.ProviderDetails.Remove(prdeitem);
            }
            db.Providers.Remove(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
