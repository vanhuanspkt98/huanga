﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaleManagement.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Data.Entity;
using System.Net;
using System.IO;

namespace SaleManagement.Controllers
{

    public class AccountController : Controller
    {
        QLBHContext db = new QLBHContext();
        ModelViewRoles md;
        [Authorize(Roles = "Admin")]
        public ActionResult Authorize()
        {
            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var ListUsers = db.Users.Select(u => new { HoTen = u.LastName + " " + u.FirstName, UserName = u.UserName, Id = u.Id }).ToList();
            var ListMV = new List<ModelViewRoles>();

            foreach (var user in ListUsers)
            {
                md = new ModelViewRoles();
                md.UserName = user.UserName;
                md.UserID = user.Id;
                if (um.IsInRole(user.Id, "Storekeeper"))
                {
                    md.IsStorekeeper = true;
                }
                else
                {
                    md.IsStorekeeper = false;
                }
                if (um.IsInRole(user.Id, "Accountant"))
                {
                    md.IsAccountant = true;
                }
                else
                {
                    md.IsAccountant = false;
                }
                ListMV.Add(md);
            }
            return View(ListMV);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult EditRole(string UserID, bool IsAccountant, bool IsStorekeeper)
        {
            //khai bao de kiem tra ket qua
            IdentityResult ir;
            //tao hai doi tuong quan ly cua EntityFramework
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            if (um.IsInRole(UserID, "Accountant"))
            {
                if (IsAccountant == false)
                {
                    ir = um.RemoveFromRole(UserID, "Accountant");
                }
            }
            else
            {
                if (IsAccountant == true)
                {
                    ir = um.AddToRole(UserID, "Accountant");
                }
            }
            if (um.IsInRole(UserID, "Storekeeper"))
            {
                if (IsStorekeeper == false)
                {
                    ir = um.RemoveFromRole(UserID, "Storekeeper");
                }
            }
            else
            {
                if (IsStorekeeper == true)
                {
                    ir = um.AddToRole(UserID, "Storekeeper");
                }
            }
            // var json = "{ success: \"true\" }";
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "Admin")]
        /*CustomCode--Huan---*/
        public ActionResult Index()
        {
            //var lssers = db.Users.Include(s => s.Department);
            return View();
        }
        //kendo
        [Authorize(Roles = "Admin")]
        public JsonResult GetGrid()
        {

            var lst = db.Users.Include(p => p.Department).Include(p => p.Branch)
             .Select(s => new
             {
                 Id = s.Id,
                 DepartmentName = s.Department.DepartmentName,
                 DepartmentID = s.DepartmentID,
                 EmployeeName = s.LastName + " "+s.MiddleName+" " + s.FirstName,
                 FirstName = s.FirstName,
                 MiddleName = s.MiddleName,
                 LastName = s.LastName,
                 Birthday = s.Birthday,
                 Sex = s.Sex,
                 Address = s.Address,
                 HireDate = s.HireDate,
                 Salary = s.Salary,
                 BranchID = s.BranchID,
                 BranchName = s.Branch.BranchName,
                 PhoneNumber = s.PhoneNumber,
                 ImageEmployee =s.ImageEmployee,
                 Email =s.Email
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        // get department
        [Authorize(Roles = "Admin")]
        public JsonResult GetgridDepartment()
        {
            var lst = db.Departments.Select(s => new
            {
                DepartmentID = s.DepartmentID,
                DepartmentName = s.DepartmentName
            });
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public JsonResult GetgridBranch()
        {
            var lst = db.Branchs.Select(s => new
            {
                BranchID = s.BranchID,
                BranchName = s.BranchName
            });
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Create(string models)
        {
            ApplicationUser employee = new ApplicationUser();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            string pass = "Password123!";
            if (ModelState.IsValid)
            {

                var user = new ApplicationUser
               {
                   //them moi nhan vien

                   FirstName = lst[0].FirstName,
                   LastName = lst[0].LastName,
                   MiddleName = lst[0].MiddleName,
                   EmailID = lst[0].Email,
                   Birthday = lst[0].Birthday,
                   Address = lst[0].Address,
                   Sex = lst[0].Sex,
                   HireDate = lst[0].HireDate,
                   Salary = lst[0].Salary,
                   DepartmentID = lst[0].DepartmentID,
                   Email = lst[0].Email,
                   PhoneNumber = lst[0].PhoneNumber,
                   UserName = lst[0].Email,
                   BranchID = lst[0].BranchID,
                   EmailConfirmed = true,
                   
                   ImageEmployee = "/Content/Employee/avatar.png"
               };
                var result = await UserManager.CreateAsync(user, pass);
                if (result.Succeeded)
                {
                    //  await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Account");
                }
                AddErrors(result);

            }
            return RedirectToAction("index", "account");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Update(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            string Id = lst[0].Id;
            var tmp = db.Users.Where(s => s.Id == Id);
            if (tmp.Count() > 0)
            {
                //cap nhat nhan vien
                var nv = db.Users.Find(Id);
                nv.Address = lst[0].Address;
                nv.Birthday = lst[0].Birthday;
                //   nv.Email = lst[0].Email;
                nv.FirstName = lst[0].FirstName;
                nv.MiddleName = lst[0].MiddleName;
                nv.LastName = lst[0].LastName;
                nv.PhoneNumber = lst[0].PhoneNumber;
                nv.Salary = lst[0].Salary;
                nv.Sex = lst[0].Sex;
                nv.HireDate = lst[0].HireDate;
                nv.DepartmentID = lst[0].DepartmentID;
                nv.BranchID = lst[0].BranchID;
                nv.Email = lst[0].Email;
                db.SaveChanges();
            }
            else
            {
                // Sai ProductID
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return RedirectToAction("index", "account");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            // db = new QLBHContext();
            dynamic lst = JsonConvert.DeserializeObject(models);
            string Id = lst[0].Id;
            var admin = db.Users.Where(s => s.Id == Id).Select(s => s.EmailID).ToList();
            if (admin.First() != "admin@tps.com")
            {
                var nv = db.Users.Find(Id);
                //xoa hoa don
                var order = nv.Orders.Select(s => s).ToList();
                foreach (var oritem in order)
                {
                    //xoa chi tiet hoa don
                    var ordetail = oritem.OrderDetails.Select(s => s).ToList();
                    foreach (var ordeitem in ordetail)
                    {
                        db.OrderDetails.Remove(ordeitem);
                    }
                    db.Orders.Remove(oritem);
                }
                db.Users.Remove(nv);
                db.SaveChanges();
                return RedirectToAction("index", "account");
            }
            return RedirectToAction("index", "account");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Upload(HttpPostedFileBase files, string models)
        {
            try
            {
                if (files != null)
                {
                    string pic = models+".png";
                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Content/Employee"), pic);
                    // file is uploaded
                    files.SaveAs(path);
                    // save to db
                  var emp=  db.Users.Find(models);
                  emp.ImageEmployee = "/Content/Employee/"+pic;
                  db.SaveChanges();
                }
            }
            catch
            {
                ViewBag.error = "File Upload Khong Hop Le !";
            }
            return RedirectToAction("");
        }
        //-----------
        [Authorize(Roles = "Admin")]
        public ActionResult Details(string Id)
        {

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(Id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Edit(string Id)
        {
            var Employ = db.Users.Find(Id);
            var mv = new RegisterViewModel()
            {
                EmployeeID = Employ.Id,
                Address = Employ.Address,
                Birthday = Employ.Birthday,
                Email = Employ.Email,
                FirstName = Employ.FirstName,
                LastName = Employ.LastName,
                MiddleName = Employ.MiddleName,
                PhoneNumber = Employ.PhoneNumber,
                Salary = Employ.Salary,
                Sex = Employ.Sex,
                HireDate = Employ.HireDate,
                DepartmentID = Employ.DepartmentID,
                BranchID = Employ.BranchID
            };
            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName", mv.DepartmentID);
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", mv.BranchID);
            return View(mv);
        }
        [Authorize(Roles = "Admin")]

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RegisterViewModel model)
        {
            var Employ = db.Users.Find(model.EmployeeID);
            Employ.Address = model.Address;
            Employ.Birthday = model.Birthday;
            Employ.Email = model.Email;
            Employ.FirstName = model.FirstName;
            Employ.LastName = model.LastName;
            Employ.MiddleName = model.MiddleName;
            Employ.PhoneNumber = model.PhoneNumber;
            Employ.Salary = model.Salary;
            Employ.Sex = model.Sex;
            Employ.HireDate = model.HireDate;
            Employ.DepartmentID = model.DepartmentID;
            Employ.BranchID = model.BranchID;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            var role = "client";
            try
            {
                var userid = db.Users.Where(s => s.EmailID == model.Email).Select(s => s.Id).FirstOrDefault();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                var rolesForUser = await userManager.GetRolesAsync(userid);
                role = rolesForUser.First();
            }
            catch { }
            string decodeUrl = "";
            switch (result)
            {
                case SignInStatus.Success:
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        decodeUrl = Server.UrlDecode(returnUrl);
                    }
                    if (Url.IsLocalUrl(decodeUrl))
                        return RedirectToLocal(returnUrl);
                    else
                    {
                        // authorize redirect
                        if (role == "Admin")
                        {
                            return RedirectToAction("index", "stats");
                        }
                        if (role == "Accountant")
                        {
                            return RedirectToAction("create", "bill");
                        }
                        if (role == "Storekeeper")
                        {
                            return RedirectToAction("index", "Product");
                        }
                        return RedirectToAction("index", "home");
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Register()
        {
            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName");
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName");

            return View();
        }

        //
        // POST: /Account/Register
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    MiddleName = model.MiddleName,
                    EmailID = model.Email,
                    Birthday = model.Birthday,
                    Address = model.Address,
                    Sex = model.Sex,
                    HireDate = model.HireDate,
                    Salary = model.Salary,
                    DepartmentID = model.DepartmentID,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    UserName = model.Email,
                    BranchID = model.BranchID
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Account");
                }
                AddErrors(result);
            }
            ViewBag.DepartmentID = new SelectList(db.Departments, "DepartmentID", "DepartmentName", model.DepartmentID);
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", model.BranchID);
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account",
                new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password",
                "Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">link</a>");
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }


        //kendo

        //------------------


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}