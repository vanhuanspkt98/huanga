﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
namespace SaleManagement.Controllers
{
    public class HomeController : Controller
    {
        QLBHContext db = new QLBHContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
         [Authorize(Roles = "Accountant, Admin")]
        public ActionResult Dashboard()
        {
           

            return View();
        }
        [Authorize(Roles = "Storekeeper, Admin")]
        public ActionResult GetNotify()
        {
            var lsPro = db.Products.Where(p => p.Quantity < 1).Select(p => p).ToList();
            return PartialView("GetNotify", lsPro);
        }
    }
}