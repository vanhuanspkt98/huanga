﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SaleManagement.Models;
using Newtonsoft.Json;

namespace SaleManagement.Controllers
{
    public class DepartmentController : Controller
    {
        private QLBHContext db = new QLBHContext();

        // GET: Department
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {

            return View();
        }
        [Authorize(Roles = "Admin")]
        public JsonResult GetGrid()
        {

            var lst = db.Departments
             .Select(s => new
             {
                 DepartmentID = s.DepartmentID,
                 DepartmentName = s.DepartmentName,
                 ManagerID = s.ManagerID,
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create(String models)
        {
            Department temp = new Department();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            temp.DepartmentID = db.Departments.Max(s => s.DepartmentID) + 1;
            temp.DepartmentName = lst[0].DepartmentName;
            temp.ManagerID = lst[0].ManagerID;
            db.Departments.Add(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Update(string models)
        {
            // temp = new temp();
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].DepartmentID;
            var temp = db.Departments.Find(Id);
            temp.DepartmentName = lst[0].DepartmentName;
            temp.ManagerID = lst[0].ManagerID;
            db.SaveChanges();
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            int Id = lst[0].DepartmentID;
            var temp = db.Departments.Find(Id);
            var nv = temp.Employees.Select(s => s).ToList();
            foreach (var nvitem in nv)
            {
                var order = nvitem.Orders.Select(s => s).ToList();
                foreach (var oderitem in order)
                {
                    var orderDetail = oderitem.OrderDetails.Select(s => s).ToList();
                    foreach (var item in orderDetail)
                    {
                        db.OrderDetails.Remove(item);
                    }
                    db.Orders.Remove(oderitem);
                }
                db.Users.Remove(nvitem);
            }
            db.Departments.Remove(temp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
