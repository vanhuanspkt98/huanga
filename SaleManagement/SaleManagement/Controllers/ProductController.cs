﻿using SaleManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
namespace SaleManagement.Controllers
{
    public class ProductController : Controller
    {
        private QLBHContext db = new QLBHContext();
        Product product;
        // GET: GetGrid
        [Authorize(Roles = "Admin,Storekeeper")]
        [HttpGet]
        public ActionResult UploadFile()
        {
           
            return View();
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase upload)
        {
            try
            {
                BinaryReader b = new BinaryReader(upload.InputStream);
                byte[] binData = b.ReadBytes((int)upload.InputStream.Length);
                string result = System.Text.Encoding.UTF8.GetString(binData).Replace("\r", "");
                string[] ar = result.Split('\n');
                for (int i = 0; i < ar.Length - 1; i++)
                {
                    string[] Produc = ar[i].Split(',');
                    var s = Produc[0];
                    Product pro = (from p in db.Products
                                   where p.ProductID == s
                                   select p).ToList().FirstOrDefault();
                    if (pro == null)
                    {
                        Product p = new Product()
                        {
                            ProductID = Produc[0],
                            ProductName = Produc[1],
                            Price = double.Parse(Produc[2]),
                            Quantity = int.Parse(Produc[3]),
                            Discount = int.Parse(Produc[4]),
                            Tax = int.Parse(Produc[5]),
                            CategoryID = Produc[6]
                        };
                        db.Products.Add(p);
                    }
                    else
                    {
                        pro.ProductID = Produc[0];
                        pro.ProductName = Produc[1];
                        pro.Price = double.Parse(Produc[2]);
                        pro.Quantity = int.Parse(Produc[3]);
                        pro.Discount = int.Parse(Produc[4]);
                        pro.Tax = int.Parse(Produc[5]);
                        pro.CategoryID = Produc[6];
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch 
            {
                ViewBag.error = "File bạn Upload co du lieu khong hop le !";
                return View("UploadFile");
            }
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public JsonResult GetGrid()
        {

            var lst = db.Products.Include(p => p.Category)
             .Select(s => new
             {
                 ProductID = s.ProductID,
                 CategoryID = s.CategoryID,
                 ProductName = s.ProductName,
                 Price = s.Price,
                 Quantity = s.Quantity,
                 Discount = ((float)s.Discount) / 100,
                 Tax = ((float)s.Tax) / 100,
                 CategoryName = s.Category.CategoryName
             }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public JsonResult GetgridCategory()
        {
            var lst = db.Categorys
                .Select(s => new
                {
                    CategoryID = s.CategoryID,
                    CategoryName = s.CategoryName
                });
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Create(String models)
        {
            product = new Product();
            //convert json from kendogrid
            dynamic lst = JsonConvert.DeserializeObject(models);
            String Id = lst[0].ProductID;
            var tmp = db.Products.Where(s => s.ProductID == Id);
            if (tmp.Count() == 0)
            {
                //them moi san pham
                product.ProductID = lst[0].ProductID;
                product.CategoryID = lst[0].CategoryID;
                product.ProductName = lst[0].ProductName;
                product.Price = lst[0].Price;
                product.Discount = lst[0].Discount;
                product.Quantity = lst[0].Quantity;
                product.Tax = lst[0].Tax;
                db.Products.Add(product);
                db.SaveChanges();
            }
            else
            {
                // cap nhat san pham
                var sp = db.Products.Find(Id);
                sp.CategoryID = lst[0].CategoryID;
                sp.ProductName = lst[0].ProductName;
                sp.Price = lst[0].Price;
                sp.Discount = lst[0].Discount;
                sp.Quantity = lst[0].Quantity;
                sp.Tax = lst[0].Tax;
                db.SaveChanges();
            }
            return RedirectToAction("index", "home");

        }
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Update(string models)
        {
            // product = new Product();

            dynamic lst = JsonConvert.DeserializeObject(models);
            String Id = lst[0].ProductID;
            var tmp = db.Products.Where(s => s.ProductID == Id);
            if (tmp.Count() > 0)
            {
                var sp = db.Products.Find(Id);
                sp.CategoryID = lst[0].CategoryID;
                sp.ProductName = lst[0].ProductName;
                sp.Price = lst[0].Price;
                sp.Discount = lst[0].Discount;
                sp.Quantity = lst[0].Quantity;
                sp.Tax = lst[0].Tax;
                db.SaveChanges();
            }
            else
            {
                // Sai ProductID
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return RedirectToAction("index", "home");

        }
       [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Destroy(string models)
        {
            dynamic lst = JsonConvert.DeserializeObject(models);
            String Id = lst[0].ProductID;
            var sp = db.Products.Find(Id);
            //remove provider details
            var prodetails = sp.ProviderDetails.Select(s => s).ToList();
            foreach (var prdeitem in prodetails)
            {
                db.ProviderDetails.Remove(prdeitem);
            }
            //remove order details
            var order = sp.OrderDetails.Select(s => s).ToList();
            foreach (var oritem in order)
            {
                db.OrderDetails.Remove(oritem);
            }
            db.Products.Remove(sp);
            db.SaveChanges();
            return RedirectToAction("index", "home");
        }

        // GET: Product/Edit/5
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categorys, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Storekeeper")]
        public ActionResult Edit([Bind(Include = "ProductID,CategoryID,ProductName,Price,Quantity,Tax,Discount")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categorys, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}