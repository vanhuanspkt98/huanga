﻿using System.Web;
using System.Web.Optimization;

namespace SaleManagement
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/angular.min.js",
                      "~/Scripts/main.js",
                      "~/Scripts/jquery-ui.js",
                // "~/Scripts/jquery-1.10.2.min.js",
                      "~/Scripts/respond.js",
                //   "~/Scripts/jquery.min.js",
                        "~/Scripts/kendo.all.min.js"
          ));


            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
          "~/Scripts/kendo/kendo.ui.core.min.js",
          "~/Scripts/kendo/kendo.dataviz.core.min.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/Admin").Include(
         "~/Scripts/Admin/jquery.slimscroll.js",
         "~/Scripts/Admin/app.js"
         ));

            //bundles.Add(new ScriptBundle("~/bundles/homepage").Include(
            //"~/Scripts/homepage/ie-emulation-modes-warning.js",
            //"~/Scripts/homepage/ie10-viewport-bug-workaround.js",
            //"~/Scripts/homepage/holder.js"));

            //style
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/Admin/css").Include(
                     "~/Content/Admin/AdminLTE.css",
                     "~/Content/Admin/font-awesome.css",
                     "~/Content/Admin/font-awesome.min.css",
                        "~/Content/Admin/skins/skin-blue-light.css"
                     ));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                "~/Content/kendo/kendo.common.min.css",
            "~/Content/kendo/kendo.dataviz.min.css",

            "~/Content/kendo/kendo.metro.min.css",
            "~/Content/kendo/kendo.dataviz.metro.min.css"
            ));
            // bundles.Add(new StyleBundle("~/Content/homepage/css").Include(
            //"~/Content/homegage/carousel.css"
            //));
            // scripts view
            bundles.Add(new ScriptBundle("~/bundles/Branch").Include(
         "~/Scripts/ScriptViews/Branch.js"
         ));
            bundles.Add(new ScriptBundle("~/bundles/Category").Include(
         "~/Scripts/ScriptViews/Category.js"
         ));
            bundles.Add(new ScriptBundle("~/bundles/Customer").Include(
         "~/Scripts/ScriptViews/Customer.js"
         ));
            bundles.Add(new ScriptBundle("~/bundles/CustomerCategory").Include(
             "~/Scripts/ScriptViews/CustomerCategory.js"
             ));
            bundles.Add(new ScriptBundle("~/bundles/Department").Include(
           "~/Scripts/ScriptViews/Department.js"
           ));
            bundles.Add(new ScriptBundle("~/bundles/Product").Include(
              "~/Scripts/ScriptViews/Product.js"
              ));
            bundles.Add(new ScriptBundle("~/bundles/Provider").Include(
             "~/Scripts/ScriptViews/Provider.js"
             ));
            bundles.Add(new ScriptBundle("~/bundles/ProviderDetail").Include(
            "~/Scripts/ScriptViews/ProviderDetail.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/Store").Include(
           "~/Scripts/ScriptViews/Store.js"
           ));
            bundles.Add(new ScriptBundle("~/bundles/Bill").Include(
           "~/Scripts/ScriptViews/Bill.js"
           ));
            bundles.Add(new ScriptBundle("~/bundles/Employee").Include(
          "~/Scripts/ScriptViews/Employee.js"
          ));
            //stats
            bundles.Add(new ScriptBundle("~/bundles/StatGraph").Include(
            "~/Scripts/ScriptViews/Stats/Graph.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/StatGraphLiquidity").Include(
            "~/Scripts/ScriptViews/Stats/GraphLiquidity.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/StatGraphProduct").Include(
            "~/Scripts/ScriptViews/Stats/GraphProduct.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/StatGraphProductQuantity").Include(
           "~/Scripts/ScriptViews/Stats/GraphProductQuantity.js"
           ));
            bundles.Add(new ScriptBundle("~/bundles/StatIndex").Include(
           "~/Scripts/ScriptViews/Stats/Index.js"
           ));
            bundles.Add(new ScriptBundle("~/bundles/StatList").Include(
          "~/Scripts/ScriptViews/Stats/List.js"
          ));
            bundles.Add(new ScriptBundle("~/bundles/StatListCustomer").Include(
          "~/Scripts/ScriptViews/Stats/ListCustomer.js"
          ));
            bundles.Add(new ScriptBundle("~/bundles/StatListDay").Include(
         "~/Scripts/ScriptViews/Stats/ListDay.js"
         ));
            bundles.Add(new ScriptBundle("~/bundles/StatListDayByDay").Include(
        "~/Scripts/ScriptViews/Stats/ListDayByDay.js"
        ));
            bundles.Add(new ScriptBundle("~/bundles/StatListMonth").Include(
       "~/Scripts/ScriptViews/Stats/ListMonth.js"
       ));
            bundles.IgnoreList.Clear();
        }
    }
}
